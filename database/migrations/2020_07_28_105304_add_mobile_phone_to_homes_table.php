<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMobilePhoneToHomesTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::table('homes', function (Blueprint $table) {
      $table->string('mobile_phone');
      $table->text('message_wa');
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::table('homes', function (Blueprint $table) {
      $table->dropColumn('mobile_phone');
      $table->dropColumn('message_wa');
    });
  }
}
