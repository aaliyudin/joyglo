<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUrlToInstagramFeedsTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::table('instagram_feeds', function (Blueprint $table) {
      $table->string('thumbnail_src');
      $table->text('user_url');
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::table('instagram_feeds', function (Blueprint $table) {
      $table->dropColumn('thumbnail_src');
      $table->dropColumn('user_url');
    });
  }
}
