<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
Route::get('/home', 'HomeController@home')->name('home');
Route::get('/', 'HomeController@index')->name('index');
Route::get('/property', 'HomeController@single')->name('single');
Route::get('/blog', 'HomeController@blogs')->name('blogs');
Route::get('/contact', 'HomeController@contact')->name('contact');
Route::get('/blogs-list', 'HomeController@blogsList')->name('blogs');
Route::get('/properties', 'HomeController@properties')->name('properties');
Route::post('/contact-us', 'HomeController@contactUs')->name('contact-us');
Route::post('/sub-email', 'HomeController@subEmail')->name('sub-email');

//Dashboard

Route::group(['prefix' => 'admin',  'middleware' => 'auth'], function() {
    Route::get('/', 'DashboardController@index')->name('dashboard');

    Route::get('/sliders', 'DashboardController@sliders')->name('dashboard-sliders');
    Route::get('/sliders-list', 'DashboardController@slidersList')->name('dashboard-sliders-list');
    Route::get('/sliders-add', 'DashboardController@slidersAdd')->name('dashboard-sliders-add');
    Route::post('/sliders-input', 'DashboardController@slidersInput')->name('dashboard-sliders-input');
    Route::get('/sliders-edit', 'DashboardController@slidersEdit')->name('dashboard-sliders-edit');
    Route::post('/sliders-update', 'DashboardController@slidersUpdate')->name('dashboard-sliders-update');
    Route::post('/sliders-delete', 'DashboardController@slidersDelete')->name('dashboard-sliders-delete');

    Route::get('/projects', 'DashboardController@projects')->name('dashboard-projects');
    Route::get('/projects-list', 'DashboardController@projectsList')->name('dashboard-projects-list');
    Route::get('/projects-add', 'DashboardController@projectsAdd')->name('dashboard-projects-add');
    Route::get('/projects-edit', 'DashboardController@projectsEdit')->name('dashboard-projects-edit');
    Route::post('/projects-input', 'DashboardController@projectsInput')->name('dashboard-projects-input');
    Route::post('/projects-update', 'DashboardController@projectsUpdate')->name('dashboard-projects-update');
    Route::post('/projects-image-delete', 'DashboardController@projectsImageDelete')->name('dashboard-projects-image-erase');
    Route::post('/projects-delete', 'DashboardController@projectsDelete')->name('dashboard-projects-delete');

    Route::get('/cities', 'DashboardController@cities')->name('dashboard-cities');
    Route::get('/cities-list', 'DashboardController@citiesList')->name('dashboard-cities-list');
    Route::get('/cities-add', 'DashboardController@citiesAdd')->name('dashboard-cities-add');
    Route::get('/cities-edit', 'DashboardController@citiesEdit')->name('dashboard-cities-edit');
    Route::post('/cities-input', 'DashboardController@citiesInput')->name('dashboard-cities-input');
    Route::post('/cities-update', 'DashboardController@citiesUpdate')->name('dashboard-cities-update');

    Route::get('/blogs', 'DashboardController@blogs')->name('dashboard-blogs');
    Route::get('/blogs-list', 'DashboardController@blogsList')->name('dashboard-blogs-list');
    Route::get('/blogs-add', 'DashboardController@blogsAdd')->name('dashboard-blogs-add');
    Route::get('/blogs-edit', 'DashboardController@blogsEdit')->name('dashboard-blogs-edit');
    Route::post('/blogs-input', 'DashboardController@blogsInput')->name('dashboard-blogs-input');
    Route::post('/blogs-update', 'DashboardController@blogsUpdate')->name('dashboard-blogs-update');
    Route::post('/blogs-delete', 'DashboardController@blogsDelete')->name('dashboard-blogs-delete');

    Route::get('/financing', 'DashboardController@financing')->name('dashboard-financing');
    Route::post('/financing-input', 'DashboardController@financingInput')->name('dashboard-financing-input');
    Route::post('/financing-image-delete', 'DashboardController@financingImageDelete')->name('dashboard-financing-image-erase');

    Route::get('/home-setting', 'DashboardController@homeSetting')->name('dashboard-home-setting');
    Route::post('/home-setting-updateinput', 'DashboardController@homeSettingUpdateInput')->name('dashboard-home-setting-updateinput');

    Route::get('/mailbox', 'DashboardController@mailbox')->name('dashboard-mailbox');
    Route::get('/mailbox-list', 'DashboardController@mailboxList')->name('dashboard-mailbox-list');
    Route::get('/mailbox-view', 'DashboardController@mailboxView')->name('dashboard-mailbox-view');
    Route::get('/mailbox-edit', 'DashboardController@mailboxEdit')->name('dashboard-mailbox-edit');
    Route::post('/mailbox-input', 'DashboardController@mailboxInput')->name('dashboard-mailbox-input');
    Route::post('/mailbox-update', 'DashboardController@mailboxUpdate')->name('dashboard-mailbox-update');
    Route::post('/mailbox-delete', 'DashboardController@mailboxDelete')->name('dashboard-mailbox-delete');
});
