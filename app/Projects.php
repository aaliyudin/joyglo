<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Projects extends Model
{
  protected $fillable = ['*'];

  public function images()
  {
    return $this->hasMany('App\ProjectImages', 'project_id');
  }

  public function slider()
  {
    return $this->hasOne('App\Projects', 'project_id');
  }
}
