<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Log;
use File;

class GetInstagram extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:get_instagram';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Instagram Feed';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      // $instaResult = file_get_contents("https://www.instagram.com/joyglo.idn/?__a=1");
      $instaResult = $this->insta_rulz("https://www.instagram.com/joyglo.idn/?__a=1");
      $insta = json_encode($instaResult, true);
      Log::info('instagram_feed');
      Log::info($insta);
      if (isset($insta['graphql']['user'])) {
        File::put(public_path('json/ig.json'),$insta);
        return true;
      } else {
        Log::info('Error getting instagram');
        return false;
      }
    }

    public function insta_rulz($url) {
      try {
        $curl_connection = curl_init();
        curl_setopt($curl_connection, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl_connection, CURLOPT_URL, $url);
        curl_setopt($curl_connection, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($curl_connection, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl_connection, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.A.B.C Safari/525.13");

        $data = json_decode(curl_exec($curl_connection), true);
        curl_close($curl_connection);
        return $data;
      } catch(Exception $e) {
        return $e->getMessage();
      }
    }
}
