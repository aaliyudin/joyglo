<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon;
use DataTables;
use Input;
use File;
use Validator;
use Redirect;
use App\SubscriptionEmail;
use App\Sliders;
use App\Blogs;
use App\Projects;
use App\ProjectImages;
use App\Financing;
use App\Homes;
use App\ContactUs;
use App\InstagramFeed;

class HomeController extends Controller
{
  /**
  * Create a new controller instance.
  *
  * @return void
  */

  /**
  * Show the application dashboard.
  *
  * @return \Illuminate\Contracts\Support\Renderable
  */
  public function index()
  {
    $projects = Projects::where('project_status', 'recent')->orderBy('created_at', 'desc')->take(9)->get();
    $sliders = Sliders::orderBy('created_at', 'desc')->take(4)->get();
    $blogs = Blogs::orderBy('created_at', 'desc')->take(3)->get();
    $financings = Financing::all();
    $userig = 'joyglo.idn';

    // $instaResult = $this->insta_rulz("https://www.instagram.com/joyglo.idn/?__a=1");
    // dd($instaResult);
    // File::put(public_path('json/ig.json'),json_encode($instaResult));
    $successProject = Projects::where('project_status', 'success')->orderBy('created_at', 'desc')->get();

    // $insta = InstagramFeed::orderBy('created_at', 'desc')->first();
    // $insta = $insta->json_code;
    // $json = json_encode($insta);
    // $insta = json_decode(File::get(public_path('json/ig.json')), true);
    // $instaArray = json_decode($json);
    // dd($instaArray);
    // return $insta['graphql'];
    $homes = Homes::latest('created_at')->first();
    $insta = InstagramFeed::all();
    // dd($instagramFeed);
    $data = [
      'projects' => $projects,
      'projects_success' => $successProject,
      'sliders' => $sliders,
      'blogs' => $blogs,
      'financings' => $financings,
      'homes' => $homes,
      'instagram_feed' => $insta
    ];
    // dd($data);
    return view('welcome')->with($data);
  }

  public function single(Request $request)
  {
    $id = $request->input('id');

    $project = Projects::find($id);
    $projects = Projects::orderBy('created_at', 'desc')->take(4)->get();
    $homes = Homes::latest('created_at')->first();
    $data = [
      'project' => $project,
      'projects' => $projects,
      'homes' => $homes
    ];

    return view('front.single')->with($data);
  }

  public function blogs(Request $request)
  {
    $id = $request->input('id');

    $blog = Blogs::find($id);
    $projects = Projects::where('project_status', 'recent')->orderBy('created_at', 'desc')->take(4)->get();
    $homes = Homes::latest('created_at')->first();
    $data = [
      'blog' => $blog,
      'projects' => $projects,
      'homes' => $homes
    ];

    return view('front.blogs')->with($data);
  }

  public function contact(Request $request)
  {
    $id = $request->input('id');

    $blog = Blogs::find($id);
    $projects = Projects::where('project_status', 'recent')->orderBy('created_at', 'desc')->take(4)->get();
    $homes = Homes::latest('created_at')->first();
    $data = [
      'blog' => $blog,
      'projects' => $projects,
      'homes' => $homes
    ];

    return view('front.contact')->with($data);
  }

  public function blogsList(Request $request)
  {
    $id = $request->input('id');

    $blogs = Blogs::paginate(8);
    $projects = Projects::where('project_status', 'recent')->orderBy('created_at', 'desc')->take(4)->get();
    $homes = Homes::latest('created_at')->first();
    $data = [
      'blogs' => $blogs,
      'projects' => $projects,
      'homes' => $homes
    ];

    return view('front.blog-list')->with($data);
  }

  public function properties(Request $request)
  {
    $id = $request->input('id');

    $blog = Blogs::all();
    $projectList = Projects::where('project_status', 'recent')->paginate(8);
    $projects = Projects::where('project_status', 'recent')->take(3)->get();
    $homes = Homes::latest('created_at')->first();
    $data = [
      'blog' => $blog,
      'projects' => $projects,
      'projectList' => $projectList,
      'homes' => $homes
    ];

    return view('front.properties')->with($data);
  }


  public function home()
  {
    return view('home');
  }

  public function insta_rulz($url) {
    try {
      $curl_connection = curl_init($url);
      curl_setopt($curl_connection, CURLOPT_CONNECTTIMEOUT, 30);
      curl_setopt($curl_connection, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($curl_connection, CURLOPT_SSL_VERIFYPEER, false);

      $data = json_decode(curl_exec($curl_connection), true);
      curl_close($curl_connection);
      return $data;
    } catch(Exception $e) {
      return $e->getMessage();
    }
  }

  public function contactUs(Request $request)
  {
    $input = $request->all();
    $rule  =  array(
      'firstname' =>  'required',
      'email'     =>  'required',
      'subject'   =>  'required',
      'message'   =>  'required'
    ) ;
    $validator = Validator::make($input,$rule);
    if ($validator->fails()) {
      $messages = $validator->messages();
      return Redirect::back()->withErrors($validator);
    } else {
      if (isset($input['lastname'])) {
        $input['lastname'] = $input['lastname'];
      } else {
        $input['lastname'] = '';
      }
      $slider = new ContactUs;
      $slider->name = $input['firstname'].' '.$input['lastname'];
      $slider->email = $input['email'];
      $slider->phone = $input['phone'];
      $slider->subject = $input['subject'];
      $slider->message = $input['message'];
      $slider->type = 'contact';
      $slider->status = 'unread';
      $slider->created_at = Carbon\Carbon::now();
      $slider->save();

      return redirect()->back()->with('message', 'Thanks for your feedback');
    }


    return view('dashboard.sliders.index');
  }

  public function subEmail(Request $request)
  {
    $input = $request->all();
    $rule  =  array(
      'email' =>  'required'
    ) ;
    $validator = Validator::make($input,$rule);
    if ($validator->fails()) {
      $messages = $validator->messages();
      return Redirect::back()->withErrors($validator);
    } else {
      $slider = new SubscriptionEmail;
      $slider->email = $input['email'];
      $slider->created_at = Carbon\Carbon::now();
      $slider->save();

      return redirect()->back()->with('message', 'Thanks, you will receive our update');
    }
  }


}
