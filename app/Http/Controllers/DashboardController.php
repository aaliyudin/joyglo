<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Carbon;
use DataTables;
use Input;
use Image;
use File;
use Log;
use App\Sliders;
use App\City;
use App\Blogs;
use App\Country;
use App\Projects;
use App\ProjectImages;
use App\Financing;
use App\ContactUs;
use App\Homes;
use App\SubscriptionEmail;
use App\InstagramFeed;

class DashboardController extends Controller
{
    public function index()
    {
      return view('dashboard.index');
    }

    public function projects()
    {
      return view('dashboard.projects.index');
    }

    public function projectsList()
    {
      return Datatables::of(Projects::all())->make(true);
    }

    public function projectsAdd()
    {
      $country = Country::all();
      $city = City::all();

      $data = [
        'city' => $city,
        'country' => $country
      ];
      return view('dashboard.projects.add')->with($data);
    }

    public function projectsInput(Request $request)
    {
      $input = $request->all();
      // return $input;
      $this->validate($request, [
        'cover' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
      ]);

      try {
        $cover = $request->file('cover');
        $coverName = $request->file('cover')->getClientOriginalName();

        $coverImage = Image::make($cover->getRealPath())->fit(506, 506);
        $coverPath = public_path() . '/cover/' . $coverName;
        $coverImage = Image::make($coverImage)->save($coverPath);

        $project = new Projects;
        $project->name = $input['name'];
        $project->address = $input['address'];
        $project->latitude = $input['latitude'];
        $project->longitude = $input['longitude'];
        $project->description = $input['description'];
        $project->country = $input['country'];
        $project->city = $input['city'];
        $project->status = $input['status'];
        $project->type = $input['type'];
        $project->price = $input['price'];
        $project->bedrooms = $input['bedrooms'];
        $project->bathrooms = $input['bathrooms'];
        $project->area = $input['area'];
        $project->building = $input['building'];
        $project->garages = $input['garages'];
        $project->year = $input['year'];
        $project->available = $input['available'];
        $project->project_status = $input['project_status'];
        $project->cover = url("cover") . '/' .$coverName;
        $project->is_active = (isset($input['is_active'])) ? true : false;
        $project->is_slideshow = (isset($input['is_slideshow'])) ? true : false;
        $project->created_at = Carbon\Carbon::now();
        $project->save();
        // dd($project->id);
        $imagesIput=array();
        if($files=$request->file('images')){
          foreach($files as $file){
            $image = Storage::disk('public')->put('/', $file);
            // dd($image);
            $imagesIput[]=[
              'project_id' => $project->id,
              'name' => $image,
              'source' => url("uploads") . '/' . $image,
              'is_active' => true,
              'is_primary' => false
            ];
          }

        }

        $projectImage = ProjectImages::insert($imagesIput);

        $cover = $request->file('cover');
        $coverName = $request->file('cover')->getClientOriginalName();

        $sliderImage = Image::make($cover->getRealPath())->fit(1920, 800);
        $sliderPath = public_path() . '/sliders/' . $coverName;
        $sliderImage = Image::make($sliderImage)->save($sliderPath);

        $slider = new Sliders;
        $slider->name = $input['name'];
        $slider->project_id = $project->id;
        $slider->description = $input['name'];
        $slider->source = url("sliders").'/'.$coverName;
        $slider->created_at = Carbon\Carbon::now();
        $slider->save();
        /*Insert your data*/
      } catch (\Exception $e) {
        return $e->getMessage();
      }

      return redirect('admin/projects');
    }

    public function projectsEdit(Request $request)
    {
      $id = $request->input('id');
      $country = Country::all();
      $city = City::all();
      $project = Projects::find($id);

      $data = [
        'id' => $id,
        'country' => $country,
        'city' => $city,
        'project' => $project
      ];

      return view('dashboard.projects.edit')->with($data);
    }

    public function projectsUpdate(Request $request)
    {
      $input = $request->all();
      // dd($input);

      if ($request->file('cover')) {
        $this->validate($request, [
          'cover' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        $cover = $request->file('cover');
        $coverName = $request->file('cover')->getClientOriginalName();

        $coverImage = Image::make($cover->getRealPath())->fit(506, 506);
        $coverPath = public_path() . '/cover/' . $coverName;
        $coverImage = Image::make($coverImage)->save($coverPath);
      }

      $project = Projects::find($input['id']);
      $project->name = $input['name'];
      $project->address = $input['address'];
      $project->latitude = $input['latitude'];
      $project->longitude = $input['longitude'];
      $project->description = $input['description'];
      $project->country = $input['country'];
      $project->city = $input['city'];
      $project->status = $input['status'];
      $project->type = $input['type'];
      $project->price = $input['price'];
      $project->bedrooms = $input['bedrooms'];
      $project->bathrooms = $input['bathrooms'];
      $project->area = $input['area'];
      $project->building = $input['building'];
      $project->garages = $input['garages'];
      $project->year = $input['year'];
      $project->available = $input['available'];
      $project->project_status = $input['project_status'];
      if ($coverName) {
        $project->cover = url("cover") . '/' .$coverName;
      }
      $project->is_active = (isset($input['is_active'])) ? true : false;
      $project->is_slideshow = (isset($input['is_slideshow'])) ? true : false;
      $project->created_at = Carbon\Carbon::now();
      $project->update();

      // dd($project->id);
      $imagesIput=array();
      if($files=$request->file('images')){
        // dd($files);
        foreach($files as $file){
          $image = Storage::disk('public')->put('/', $file);
          // dd($image);
          $imagesIput[]=[
            'project_id' => $input['id'],
            'name' => $image,
            'source' => url("uploads") . '/' . $image,
            'is_active' => true,
            'is_primary' => false
          ];
          $projectImage = ProjectImages::insert($imagesIput);
        }

      }
      return redirect('admin/projects');

    }

    public function projectsDelete(Request $request)
    {
      $id = $request->input('id');

      $project = Projects::find($id);

      $project->delete();

      $slider = Sliders::where('project_id', $id)->delete();

      return 'true';
    }

    public function projectsImageDelete(Request $request)
    {
      $id = $request->input('id');

      $projectImage = ProjectImages::find($id);

      $projectImage->delete();

      return 'true';
    }

    public function sliders(Request $request)
    {
      return view('dashboard.sliders.index');
    }

    public function slidersList(Request $request)
    {
      return Datatables::of(Sliders::with('project')->get())->make(true);
    }

    public function SlidersAdd()
    {
      $projects = Projects::all();
      $data = [
        'projects' => $projects
      ];
      return view('dashboard.sliders.add')->with($data);
    }

    public function SlidersInput(Request $request)
    {
      $input = $request->all();

      $this->validate($request, [
        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
      ]);

      $image = $request->file('image');
      $nameImage = $request->file('image')->getClientOriginalName();

      $sliderImage = Image::make($image->getRealPath())->fit(1920, 800);
      $sliderPath = public_path() . '/sliders/' . $nameImage;
      $sliderImage = Image::make($sliderImage)->save($sliderPath);

      // $oriPath = public_path() . '/normal_images/' . $nameImage;
      // $oriImage = Image::make($image)->save($oriPath);

      $slider = new Sliders;
      $slider->name = $input['name'];
      $slider->project_id = $input['project'];
      $slider->description = $input['name'];
      $slider->source = url("sliders").'/'.$nameImage;
      $slider->created_at = Carbon\Carbon::now();
      $slider->save();

      return view('dashboard.sliders.index');
    }

    public function SlidersEdit(Request $request)
    {
      $id = $request->input('id');
      $slider = Sliders::find($id);
      $projects = Projects::all();
      $data = [
        'slider' => $slider,
        'projects' => $projects
      ];
      return view('dashboard.sliders.edit')->with($data);
    }

    public function SlidersUpdate(Request $request)
    {
      $input = $request->all();

      $this->validate($request, [
        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
      ]);

      $image = $request->file('image');
      $nameImage = $request->file('image')->getClientOriginalName();

      $sliderImage = Image::make($image->getRealPath())->fit(1920, 800);
      $sliderPath = public_path() . '/sliders/' . $nameImage;
      $sliderImage = Image::make($sliderImage)->save($sliderPath);

      // $oriPath = public_path() . '/normal_images/' . $nameImage;
      // $oriImage = Image::make($image)->save($oriPath);

      $project = Sliders::find($input['id']);
      $project->name = $input['name'];
      $project->project_id = $input['project'];
      $project->description = $input['name'];
      $project->source = url("sliders").'/'.$nameImage;
      $project->created_at = Carbon\Carbon::now();
      $project->update();

      return redirect('admin/sliders');
    }

    public function slidersDelete(Request $request)
    {
      $id = $request->input('id');

      $slider = Sliders::find($id);

      $slider->delete();

      return 'true';
    }

    public function cities()
    {
      return view('dashboard.cities.index');
    }

    public function citiesList()
    {
      return Datatables::of(City::all())->make(true);
    }

    public function citiesAdd()
    {
      return view('dashboard.cities.add');
    }

    public function citiesInput(Request $request)
    {
      $input = $request->all();

      $city = new City;
      $city->name = $input['name'];
      $city->created_at = Carbon\Carbon::now();
      $city->save();

      return view('dashboard.cities.index');
    }

    public function citiesEdit(Request $request)
    {
      $id = $request->input('id');

      $city = City::find($id);

      return view('dashboard.cities.edit')->with('city', $city);
    }

    public function citiesUpdate(Request $request)
    {
      $input = $request->all();

      $city = City::find($input['id']);
      $city->name = $input['name'];
      $city->updated_at = Carbon\Carbon::now();
      $city->save();

      return view('dashboard.cities.index');
    }

    public function blogs()
    {
      return view('dashboard.blogs.index');
    }

    public function blogsList()
    {
      return Datatables::of(Blogs::all())->make(true);
    }

    public function blogsAdd()
    {
      return view('dashboard.blogs.add');
    }

    public function blogsInput(Request $request)
    {
      $input = $request->all();
      // dd($input);
      $this->validate($request, [
        'images' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
      ]);

      $images = $request->file('images');
      $nameImage = $request->file('images')->getClientOriginalName();

      $sliderImage = Image::make($images->getRealPath())->fit(1920, 800);
      $sliderPath = public_path() . '/blogs/' . $nameImage;
      $sliderImage = Image::make($sliderImage)->save($sliderPath);

      $project = new Blogs;
      $project->title = $input['title'];
      $project->content = $input['content'];
      $project->images = url("blogs").'/'.$nameImage;
      $project->created_at = Carbon\Carbon::now();
      $project->save();

      return view('dashboard.blogs.index');
    }

    public function blogsEdit(Request $request)
    {
      $id = $request->input('id');

      $blog = Blogs::find($id);

      return view('dashboard.blogs.edit')->with('blog', $blog);
    }

    public function blogsUpdate(Request $request)
    {
      $input = $request->all();

      $city = Blogs::find($input['id']);
      $city->name = $input['name'];
      $city->updated_at = Carbon\Carbon::now();
      $city->save();

      return view('dashboard.blogs.index');
    }

    public function blogsDelete(Request $request)
    {
      $id = $request->input('id');

      $blog = Blogs::find($id);

      $blog->delete();

      return 'true';
    }

    public function financing(Request $request)
    {
      $financings = Financing::all();
      $data = [
        'financings' => $financings
      ];

      return view('dashboard.financing.index')->with($data);
    }

    public function financingInput(Request $request)
    {
      $imagesIput=array();
      if($files=$request->file('images')){
        // dd($files);
        foreach($files as $file){
          $image = Storage::disk('public')->put('/', $file);
          // dd($image);
          $imagesIput[]=[
            'name' => $image,
            'src' => url("uploads") . '/' . $image,
          ];
          $projectImage = Financing::insert($imagesIput);
        }
        return redirect('admin/financing');
      }

      return redirect('admin/financing');
    }

    public function financingImageDelete(Request $request)
    {
      $id = $request->input('id');

      $projectImage = Financing::find($id);

      $projectImage->delete();

      return 'true';
    }

    public function mailbox(Request $request)
    {
      $mailbox = ContactUs::paginate(10);
      $data = [
        'mailbox' => $mailbox
      ];
      return view('dashboard.mails.index')->with($data);
    }

    public function mailboxList(Request $request)
    {
      return Datatables::of(ContactUs::all())->make(true);
    }

    public function mailboxView(Request $request)
    {
      $id = $request->input('id');

      $mail = ContactUs::find($id);
      $data = [
        'mails' => $mail
      ];

      return view('dashboard.mails.view')->with($data);
    }

    public function mailboxDelete(Request $request)
    {
      $id = $request->input('id');

      $project = ContactUs::find($id);

      $project->delete();

      return 'true';
    }

    public function homeSetting(Request $request)
    {
      $homes = Homes::latest('created_at')->first();
      $insta = InstagramFeed::all();
      $data = [
        'homes' => $homes,
        'ig' => $insta
      ];

      return view('dashboard.homes.index')->with($data);
    }

    public function homeSettingUpdateInput(Request $request)
    {
      $input = $request->all();
      $validator = Validator::make($request->all(), [
        'background_home' => 'image|mimes:jpeg,png,jpg,gif,svg',
        'address' => 'required',
        'email' => 'required',
        'social_facebook' => 'required',
        'social_twitter' => 'required',
        'mobile_phone' => 'required',
        'message_wa' => 'required',
      ]);

      if ($validator->fails()) {
        return redirect('admin/home-setting')->withErrors($validator)->withInput();
      }

      if ( $request->file('background_home')) {
        $imagesHome = $request->file('background_home');
        $nameImageHome = $request->file('background_home')->getClientOriginalName();

        $homeImage = Image::make($imagesHome->getRealPath())->fit(1920, 800);
        $homePath = public_path() . '/homes/' . $nameImageHome;
        $homeImage = Image::make($homeImage)->save($homePath);

        $imagesOther = $request->file('background_other');
        $nameImageOther = $request->file('background_other')->getClientOriginalName();

        $otherImage = Image::make($imagesOther->getRealPath())->fit(1920, 800);
        $otherPath = public_path() . '/others/' . $nameImageOther;
        $otherImage = Image::make($otherImage)->save($otherPath);
        $bgHome =  url("homes").'/'.$nameImageHome;
        $bgOther = url("others").'/'.$nameImageOther;
      } else {
        $homes = Homes::latest('created_at')->first();
        $bgHome =  $homes->background_home;
        $bgOther =  $homes->background_other;
      }

      if ($input['json_code']) {
        $insta = json_decode($input['json_code'], true);
        Log::info('instagram_feed');
        Log::info($insta);
        if (isset($insta['graphql']['user'])) {
          // $insta = json_encode($insta['graphql']['user']['edge_owner_to_timeline_media']['edges']);
          // dd($insta['graphql']['user']['edge_owner_to_timeline_media']['edges']);
          InstagramFeed::truncate();
          $instaArray = $insta['graphql']['user']['edge_owner_to_timeline_media']['edges'];
          foreach ($instaArray as $key => $inst) {
            $insertJsonIg = InstagramFeed::insert([
              'json_code' => 'json',
              'thumbnail_src' => $inst['node']['thumbnail_src'],
              'user_url' => 'https://www.instagram.com/joyglo.idn'
            ]);
          }
          // file_put_contents(public_path('json/ig.json') $insta);
        } else {
          Log::info('Error getting instagram');
        }
      }
      // return $input;

      $homes = new Homes;
      $homes->address = $input['address'];
      $homes->email = $input['email'];
      $homes->background_home = $bgHome;
      $homes->background_other = $bgOther;
      $homes->social_facebook = $input['social_facebook'];
      $homes->social_twitter = $input['social_twitter'];
      $homes->mobile_phone = $input['mobile_phone'];
      $homes->message_wa = $input['message_wa'];
      $homes->created_at = Carbon\Carbon::now();
      $homes->save();

      return redirect('admin/home-setting');
    }
}
