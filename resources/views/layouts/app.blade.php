<!doctype html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
	<!-- Meta Tag -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Joyglo">
	<meta name="keywords" content="real estate, property, property search, agent, apartments, booking, business, idx, housing, real estate agency, rental">
	<meta name="author" content="Joyglo">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>Joyglo</title>
	<!-- Favicon -->
	<link rel="shortcut icon" href="front/img/favicon.ico">

	<!-- Required style of the theme -->

	<!--===============================================================================================-->
	<link rel="stylesheet" href="front/css/bootstrap-4.1.3.min.css">
	<link rel="stylesheet" href="front/css/bootstrap-select.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" href="front/css/fontawesome-all-5.5.0.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" href="front/css/style.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" href="front/css/layerslider.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" href="front/css/color.css" id="color-change">
	<!--===============================================================================================-->
	<link rel="stylesheet" href="front/css/jslider.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" href="front/css/responsive.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" href="front/css/loader.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" href="front/css/settings.css">
	<!--===============================================================================================-->
	@yield('css')

</head>

<body class="page-load">
	<div class="preloader">
		<div class="loading-page">
			<div class="sk-circle">
				<div class="sk-circle1 sk-child"></div>
				<div class="sk-circle2 sk-child"></div>
				<div class="sk-circle3 sk-child"></div>
				<div class="sk-circle4 sk-child"></div>
				<div class="sk-circle5 sk-child"></div>
				<div class="sk-circle6 sk-child"></div>
				<div class="sk-circle7 sk-child"></div>
				<div class="sk-circle8 sk-child"></div>
				<div class="sk-circle9 sk-child"></div>
				<div class="sk-circle10 sk-child"></div>
				<div class="sk-circle11 sk-child"></div>
				<div class="sk-circle12 sk-child"></div>
			</div>
		</div>
	</div>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<a href="https://api.whatsapp.com/send?phone=+{{$homes->mobile_phone}}&text={{$homes->message_wa}}" class="float" target="_blank">
		<i class="fa fa-whatsapp my-float"></i>
	</a>
	<!-- Wrapper Start -->
	<div id="page_wrapper" class="container-fluid">
		<div class="row">
			<header id="header" class="w-100 bg_white nav-on-top">

				<!-- Nav Header Start -->
				<div class="main_header_1">
					<div class="container">
						<div class="row">
							<div class="col-md-12 col-sm-12">
								<nav class="navbar navbar-expand-lg navbar-light w-100">
									<a class="navbar-brand" href="{{url('/')}}"><img class="nav-logo" src="{{asset('front/img/logo.jpg')}}" alt="logo" style="width:75px;"></a>
									<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
									<div class="collapse navbar-collapse" id="navbarSupportedContent">
										<ul class="navbar-nav ml-auto">
											<li class="nav-item">
												<a class="nav-link" href="{{url('/')}}">Home</a>
											</li>
											<li class="nav-item">
												<a class="nav-link" href="{{route('properties')}}">Properties</a>
											</li>
											<li class="nav-item">
												<a class="nav-link" href="{{route('blogs')}}">News</a>
											</li>
											<li class="nav-item">
												<a class="nav-link" href="{{url('/')}}#financing">Financing</a>
											</li>
											<li class="nav-item">
												<a class="nav-link" href="{{url('/')}}#feed">Instagram Feed</a>
											</li>
										</ul>
										<div class="submit_property ml-2"><a class="btn btn_primary_bg" href="{{route('contact')}}">Contact Us</a></div>
									</div>
								</nav>
							</div>
						</div>
					</div>
				</div>
				<!-- Nav Header End -->
			</header><!-- Slider HTML markup -->

			@yield('content')

			<!-- Footer Section Start -->
			<section class="full_row py_80 bg_secondary">
				<div class="container">
					<div class="row">
						<div class="col-lg-3 col-sm-6">
							<div class="footer_widget company_info">
								<div class="widget_head">
									<a class="navbar-brand" href="{{url('/')}}"><img class="nav-logo" src="{{asset('front/img/logo.jpg')}}" alt="logo" style="width:75px;"></a>
								</div>
								<p></p>
								<div class="socail_media_1 mt-3">
									<ul>
										<li><a href="{{$homes->social_facebook}}"><i class="fab fa-facebook-f"></i></a></li>
										<li><a href="{{$homes->social_twitter}}"><i class="fab fa-twitter"></i></a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-sm-6">
							<div class="footer_widget get_in_touch">
								<div class="widget_head">
									<h4 class="widget_title">Get In Touch</h4>
								</div>
								<ul>
									<li> <i class="fas fa-map-marker-alt"></i>
										<h6 class="text_white mb-2">Office Address</h6>
										<span>{{$homes->address}}</span>
									</li>
									<!-- <li> <i class="fa fa-phone" aria-hidden="true"></i>
									<h6 class="text_white mb-2">Call Us 24/7</h6>
									<span>(+241) 542 34251, (+241) 234 88232</span>
								</li> -->
								<li> <i class="far fa-envelope"></i>
									<h6 class="text_white mb-2">Email Address</h6>
									<span>{{$homes->email}}</span>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-lg-3 col-sm-6">
						<!-- <div class="footer_widget quick_link">
						<div class="widget_head">
						<h4 class="widget_title">Quick Links</h4>
					</div>
					<ul>
					<li><a href="faq.html">Freequinly Ask Question</a></li>
					<li><a href="about.html">About Our Company</a></li>
					<li><a href="our_service.html">Our Professional Services</a></li>
					<li><a href="terms_and_condition.html">Terms and Conditions</a></li>
					<li><a href="submit_property.html">Submit Your Property</a></li>
					<li><a href="#">Become A Member</a></li>
				</ul>
			</div> -->
		</div>
		<div class="col-lg-3 col-sm-6">
			<div class="footer_widget newslatter">
				<div class="widget_head">
					<h4 class="widget_title">Newslatter</h4>
				</div>
				<p>Subscribe to our newsletter and we will inform your about newset projects.</p>
				<div class="news_letter">
					@if($errors->any())
					<div class="alert alert-danger">{{$errors->first()}}</div>
					@endif
					@if(session()->has('message'))
					<div class="alert alert-success">
						{{ session()->get('message') }}
					</div>
					@endif
					<form action="{{route('sub-email')}}" method="post">
						{{csrf_field()}}
						<input type="email" name="email" placeholder="Enter Your Email" class="form-control">
						<button type="submit" name="submit" class="btn btn_primary">subscribe</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
</section>
<!-- Footer Section End -->

<!-- Bottom Footer Start -->
<div class="full_row py-4 bg_black">
	<span class="copyright_text text-center text_white d-block">Copyright &copy; 2019 Joyglo All Right Reserve</span>
</div>
<!-- Bottom Footer End -->

<!-- Find Part Satrt -->

<!-- Find Part Satrt -->
</div>
</div>
<!-- Wrapper End -->

<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<!--===============================================================================================-->
<script src="front/js/jquery-3.3.1.min.js"></script>
<script src="front/js/greensock.js"></script>
<!-- LayerSlider script files -->
<script src="front/js/layerslider.transitions.js"></script>
<script src="front/js/layerslider.kreaturamedia.jquery.js"></script>
<!--===============================================================================================-->
<script src="front/js/popper-1.14.3.min.js"></script>
<script src="front/js/bootstrap-4.1.3.min.js"></script>
<script src="front/js/bootstrap-select.min.js"></script>
<!--===============================================================================================-->
<script src="front/js/YouTubePopUp.jquery.js"></script>
<!--===============================================================================================-->
<script src="front/js/jquery.fancybox.pack.js"></script>
<script src="front/js/jquery.fancybox-media.js"></script>
<!--===============================================================================================-->
<!-- Owl Carousel slide need-->
<script src="front/js/owl.js"></script>
<!--===============================================================================================-->
<!-- Achievement Counting need-->
<script src="front/js/nsc.js"></script>
<!--===============================================================================================-->
<!--tab gallery section-->
<script src="front/js/mixitup.js"></script>
<!--===============================================================================================-->
<script src="front/js/wow.js"></script>
<!--===============================================================================================-->
<script src="front/js/jquery.cookie.js"></script>
<script src="front/js/settings.js"></script>
<!--===============================================================================================-->
<script src="front/js/jshashtable-2.1_src.js"></script>
<!--===============================================================================================-->
<script src="front/js/jquery.numberformatter-1.2.3.js"></script>
<!--===============================================================================================-->
<script src="front/js/tmpl.js"></script>
<!--===============================================================================================-->
<script src="front/js/jquery.dependClass-0.1.js"></script>
<!--===============================================================================================-->
<script src="front/js/draggable-0.1.js"></script>
<!--===============================================================================================-->
<script src="front/js/jquery.slider.js"></script>
<!--===============================================================================================-->
<script src="front/js/map/custom-map.js"></script>
<!--===============================================================================================-->
<script src="front/js/jquery.barrating.js"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3&amp;sensor=false&key=AIzaSyAg89gOFEbEuy9Aq5UQ_chZnDve13T21Ew"></script>
@yield('js')
<script src="front/js/custom.js"></script>
</body>

</html>
