@extends('layouts.app')
@section('css')
  <style media="screen">
    .ls-fullscreen-wrapper {
      z-index: 1 !important;
    }
    .bg_img_3 {
      background-image: url({{$homes->background_home}}) !important;
    }
  </style>
@endsection
@section('content')
<div class="full_row">
  <div id="slider" class="overflow_hidden" style="width:1200px; height:640px; margin:0 auto;margin-bottom: 0px; z-index: 1 !important;">
    @foreach($sliders as $slider)
    <div class="ls-slide" data-ls="bgsize:cover; bgposition:50% 50%; duration:4000; transition2d:104; kenburnsscale:1.00;">
      <img width="1920" height="800" src="{{$slider->source}}" class="ls-bg" alt="" />

      <div style="width:100%; height:100%; background:rgba(11, 15, 41, 0.49); top:50%; left:50%;" class="ls-l" data-ls="easingin:easeOutQuint; durationout:300; parallaxlevel:0; position:fixed;"></div>

      <p style="font-size:18px; font-weight:500; top:280px; left:40px; font-family: 'Montserrat', sans-serif;" class="ls-l text_white" data-ls="offsetyin:100%; offsetyout:-250; delayin:100; clipin:0 0 100% 0; durationout:400; parallaxlevel:0;">Rp {{number_format($slider->project->price)}}</p>

      <p style="top:300px; left:40px; text-align:initial; font-weight:600; font-style:normal; font-family:Montserrat; text-decoration:none; font-size:30px; line-height:76px; width:600px; white-space:normal;" class="ls-l text_white" data-ls="offsetyin:0; offsetxin:-110w; delayin:300; easingin:easeOutQuint; offsetyout:-250; fadein:false; clipin:0 0 0 100%; durationout:400; parallax:false;">{{$slider->project->name}}</p>

      <p style="font-weight:400; text-align:left; width:500px; font-family:Montserrat; font-size:15px; line-height:30px; top:370px; left:40px; white-space:normal;" class="ls-l text_white" data-ls="offsetyin:0; offsetxin:-110w; delayin:800; easingin:easeOutQuint; offsetyout:-250; fadein:false; clipin:0 0 0 100%; durationout:300; parallax:false;"><i class="fas fa-map-marker-alt"></i> {{$slider->project->address}}</p>

      <a style="" class="ls-l" href="{{url('property?id='.$slider->project->id)}}" target="_self" data-ls="offsetyin:40; delayin:1200; easingin:easeOutQuint; offsetyout:-300; durationout:400; hover:true; hoverdurationin:300; hoveropacity:1; hoverbgcolor:#ffffff; hovercolor:#444444; parallax:false;">
        <p style="font-weight:500; text-align:center; cursor:pointer; padding-right:30px; padding-left:30px; font-size:14px; line-height:30px; top:440px; left:40px; padding-top:10px; padding-bottom:10px; white-space:normal;" class="btn btn_primary">View Details</p>
      </a>
    </div>
    @endforeach

  </div>
</div>

<!-- Recent Property Start -->
<section class="full_row py_80 bg_gray">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3 class="d-table float-left mb_30">Recent <span class="text_primary">Projects</span></h3>
        <a href="{{route('properties')}}" class="property_link float-right">View All Projects</a>
      </div>
    </div>

    <div class="row">
      @foreach($projects as $value)
      <div class="col-lg-3 col-md-6">
        <div class="property_grid_1 property_item bg-white mb_30">
          <div class="zoom_effect_1">
            <div class="upper_1 bg_secondary text-white">{{$value->status}}</div>
            <a href="{{url('property?id='.$value->id)}}">
              <div class="thumb-wrapper">
                <div class="thumb-image">
                  @if(strlen($value->cover) > 0)
                  <img src="{{$value->cover}}" height="100%" width="auto" alt="Image Not Found!">
                  @else
                  <img src="{{$value->images[0]->source}}" height="100%" width="auto" alt="Image Not Found!">
                  @endif
                </div>
              </div>
            </a>
          </div>
          <div class="property_text p-3">
            <h5 class="title"><a href="single_property.html">{{$value->name}}</a></h5>
            <span class="my-3 d-block"><i class="fas fa-map-marker-alt"></i> {{$value->address}} </span>
            <div class="quantity">
              <ul>
                <li><span>Land Size</span>{{$value->area}} M2</li>
                <li><span>Beds</span>{{$value->bedrooms}}</li>
                <li><span>Baths</span>{{$value->bathrooms}}</li>
                <li><span>Garage</span>{{$value->garages}}</li>
              </ul>
            </div>
          </div>
          <div class="bed_area d-table w-100">
            <ul>
              <li>Rp {{number_format($value->price)}}</li>
              <li class="icon_medium">
                <a href="#"><i class="flaticon-like"></i></a>
              </li>
              <li class="icon_medium">
                <a href="#"><i class="flaticon-connections"></i></a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      @endforeach
    </div>
  </div>
</section>
<!-- Recent Property End -->

<!-- info Banner -->
<section class="full_row py_80 overlay_1 bg_img_3">
  <div class="container">
    <div class="row">
      <div class="banner_1 text-center"> <span class="text_white">Do you want to buy our property ?</span>
        <h2 class="title text_white my-4 text-capitalize">We help you to find a best property for your</h2>
        <a class="btn btn_primary" href="{{route('properties')}}">Find Property</a>
      </div>
    </div>
  </div>
</section>
<!-- info Banner End -->
@if($projects_success->count() > 0)
<section class="full_row py_80 bg_gray">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3 class="d-table float-left mb_30">Successfull <span class="text_primary">Projects</span></h3>
        <a href="{{route('properties')}}" class="property_link float-right">View Projects</a>
      </div>
    </div>
    <div class="row">
      @foreach($projects_success as $value)
      <div class="col-lg-3 col-md-6">
        <div class="property_grid_1 property_item bg-white mb_30">
          <div class="zoom_effect_1">
            <div class="upper_1 bg_secondary text-white">{{$value->status}}</div>
            <a href="{{url('property?id='.$value->id)}}">
              <div class="thumb-wrapper">
                <div class="thumb-image">
                  @if(strlen($value->cover) > 0)
                  <img src="{{$value->cover}}" height="100%" width="auto" alt="Image Not Found!">
                  @else
                  <img src="{{$value->images[0]->source}}" height="100%" width="auto" alt="Image Not Found!">
                  @endif
                </div>
              </div>
            </a>
          </div>
          <div class="property_text p-3">
            <h5 class="title"><a href="single_property.html">{{$value->name}}</a></h5>
            <span class="my-3 d-block"><i class="fas fa-map-marker-alt"></i> {{$value->address}} </span>
            <div class="quantity">
              <ul>
                <li><span>Land Size</span>{{$value->area}} M2</li>
                <li><span>Beds</span>{{$value->bedrooms}}</li>
                <li><span>Baths</span>{{$value->bathrooms}}</li>
                <li><span>Garage</span>{{$value->garages}}</li>
              </ul>
            </div>
          </div>
          <div class="bed_area d-table w-100">
            <ul>
              <li>Rp {{number_format($value->price)}}</li>
              <li class="icon_medium">
                <a href="#"><i class="flaticon-like"></i></a>
              </li>
              <li class="icon_medium">
                <a href="#"><i class="flaticon-connections"></i></a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      @endforeach
    </div>
  </div>
</section>
@endif

<section class="full_row py_80 bg_white">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="title_row text-center mb-5">
          <h3 class="title mb-4">Recent <span class="text_primary">News</span></h3>
          <div class="sub_title text_secondary">
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      @foreach($blogs as $blog)
      <div class="col-lg-4 col-md-6">
        <div class="news_item_1 bg_white mb_30 wow fadeInUp animated" data-wow-delay="100ms" data-wow-duration="1000ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 100ms; animation-name: fadeInUp;">
          <div class="news_img"> <img src="{{$blog->images}}" alt="Image Not Found!"> <a href="{{url('blog?id='.$blog->id)}}" class="news_eye"><i class="far fa-eye"></i></a> </div>
          <div class="details p_20">
            <h5 class="title mb-2"><a href="{{url('blog?id='.$blog->id)}}">{{$blog->title}}</a></h5>
            <div class="post_info">Posted By <a href="#" class="text_primary">Joyglo</a> On {{$blog->created_at}}</div>
            <a class="btn_link" href="{{url('blog?id='.$blog->id)}}">Read More</a>
          </div>
        </div>
      </div>
      @endforeach
    </div>
  </div>
</section>

<!-- Team Section Start -->
<!-- <section class="full_row py_80 bg_gray">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3 class="d-table float-left mb_30">Our <span class="text_primary">Financing</span></h3>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="owl-carousel owl_carousel_2">
          <div class="team_member_1 bg_white text-center">
            <div class="zoom_effect_1">
              <a href="agent_profile_grid.html"><img src="front/img/teams/2.jpg" alt="Image Not Found!"></a>
            </div>
          </div>
          <div class="team_member_1 bg_white text-center">
            <div class="zoom_effect_1">
              <a href="agent_profile_grid.html"><img src="front/img/teams/3.jpg" alt="Image Not Found!"></a>
            </div>
          </div>
          <div class="team_member_1 bg_white text-center">
            <div class="zoom_effect_1">
              <a href="agent_profile_grid.html"><img src="front/img/teams/4.jpg" alt="Image Not Found!"></a>
            </div>
          </div>
          <div class="team_member_1 bg_white text-center">
            <div class="zoom_effect_1">
              <a href="agent_profile_grid.html"><img src="front/img/teams/5.jpg" alt="Image Not Found!"></a>
            </div>
          </div>
          <div class="team_member_1 bg_white text-center">
            <div class="zoom_effect_1">
              <a href="agent_profile_grid.html"><img src="front/img/teams/6.jpg" alt="Image Not Found!"></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section> -->
<!-- Team Section End -->
@if($financings->count() > 0)
<!-- Team Section Start -->
<section class="full_row py_80 bg_grey" id="financing">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3 class="d-table float-left mb_30"><span class="text_primary">Financing</span></h3>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="owl-carousel owl_carousel_fi">
          @foreach($financings as $financing)
          <div class="team_member_1 bg_white text-center">
            <div class="zoom_effect_1">
              <a href="#" target="_blank"><img src="{{$financing->src}}" alt="Image Not Found!"></a>
            </div>
          </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Team Section End -->
@endif
<!-- Team Section Start -->
<section class="full_row py_80 bg_white" id="feed">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3 class="d-table float-left mb_30">Instagram <span class="text_primary">Feed</span></h3>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="owl-carousel owl_carousel_ig">
          @foreach($instagram_feed as $value)
          <div class="team_member_1 bg_white text-center">
            <div class="zoom_effect_1">
              <a href="https://www.instagram.com/joyglo.idn/" target="_blank"><img src="{{$value->thumbnail_src}}" alt="Image Not Found!"></a>
            </div>
          </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Team Section End -->
@endsection
