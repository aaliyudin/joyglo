@extends('layouts.app')
@section('css')
<link rel="stylesheet" href="front/css/ekko-lightbox.css">
<style media="screen">
  .infoBox {
    overflow: inherit!important;
    position: relative;
    border: 5px solid rgba(0,0,0,.05);
    box-shadow: 0 5px 13px rgba(0,0,0,.22);
  }
  .infoBox>img{
    z-index: 5;
    padding: 3px 3px 0 0;
    width: auto!important;
  }
  .infoBox .marker-content {
    background-color: #fff;
    padding: 22px 20px;
    text-align: left;
  }
  .infoBox .marker-content-item {
    overflow: hidden;
  }
  .marker-content:after {
    border: solid transparent;
    bottom: -40px;
    z-index: 2;
    border-width: 20px;
    border-top-color: #fff;
    -moz-transform: translate(-50%,0);
    -o-transform: translate(-50%,0);
    -ms-transform: translate(-50%,0);
    -webkit-transform: translate(-50%,0);
    transform: translate(-50%,0);
  }
  .marker-content:after {
    content: ' ';
    height: 0;
    position: absolute;
    top: inherit;
    right: inherit;
    left: 50%;
    width: 0;
  }
  .infoBox:after, #map-property-single .marker-content:after {
    content: ' ';
    height: 0;
    position: absolute;
    top: inherit;
    right: inherit;
    left: 50%;
    width: 0;
  }
  .infoBox .item-thumb img {
    float: left;
  }
  .infoBox .item-body .address-marker {
    font-size: 12px;
    color: #8f8f8f;
  }
  .infoBox .item-body .price-marker {
    color: #222;
  }
  .infoBox .item-body .price-marker, #map-property-single .item-body .title-marker {
    font-size: 14px;
    font-weight: 500;
    line-height: 1.4;
    letter-spacing: 0;
    color: #222;
    text-transform: capitalize;
  }
  .infoBox .item-body .title-marker {
    font-size: 14px;
    font-weight: 500;
    line-height: 1.4;
    letter-spacing: 0;
    color: #222;
    text-transform: capitalize;
  }
  .infoBox .item-body a:hover {
    color: #959686;
  }
  .infoBox .item-thumb {
    display: inline-block;
    float: left;
    margin-right: 12px;
  }
  .infoBox .item-body .address-marker i {
    margin-right: 10px;
  }
</style>
@endsection
@section('content')
<!-- Banner Section Start -->
<section class="full_row bg_img_5 py_80 overlay_1 page-banner">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="page_banner">
          <h3 class="title text_white d-table float-left">{{$project->name}}</h3>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Banner Section End -->

<!-- Single Property Start -->
<section class="full_row pt_80 bg_gray">
  <div class="container">
    <div class="row">
      <div class="col-lg-8">
        <div class="row">
          <div class="col-md-12">
            <div id="gallery">

              <div id="panel">
                <img id="largeImage" src="{{$project->images[0]->source}}" />
              </div>
              <div class="owl-carousel owl_carousel_sl">
                @foreach($project->images as $value)
                <div class="overflow-hide">
                  <a href="{{$value->source}}" data-toggle="lightbox" data-gallery="gallery">
                    <img src="{{$value->source}}" alt="1st image description" />
                  </a>
                </div>
                @endforeach
              </div>

            </div>

            <!-- <div class="property_slider">
            <img src="{{$project->images[0]->source}}" alt="">
          </div> -->
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 bg-white">
          <div class="single_property_detail">
            <div class="head">
              <h4 class="title mt-4">{{$project->name}}</h4>
              <span class=""><i class="fas fa-map-marker-alt"></i> {{$project->address}} </span>
            </div>
            <p>{!!$project->description!!}</p>
            <div class="single_map mb-5">
              <h4 class="inner-title mb-4">Location</h4>
              <div id="googleMap" style="height: 400px;"></div>
              <input type='hidden' value="{{$project->latitude}}" name='latitude' id='lat'>
              <input type='hidden' value="{{$project->longitude}}" name='longitude' id='lng'>
            </div>
            <div class="mb-5">
              <h4 class="inner-title mb-4">More Information</h4>
              <div class="tab-content hidden-xs">
                <div id="ere-overview" class="tab-pane fade in active">
                  <ul class="list-2-col ere-property-list">
                    <li>
                      <strong>Property ID</strong>
                      <span>{{$project->id}}</span>
                    </li>
                    <li>
                      <strong>Price</strong>
                      <span class="ere-property-price">Rp. {{number_format($project->price)}}</span>
                    </li>
                    <li>
                      <strong>Property Type</strong>
                      <span>{{$project->type}}</span>
                    </li>
                    <li>
                      <strong>Property status</strong>
                      <span>For {{$project->status}}</span>
                    </li>
                    <li>
                      <strong>Bedrooms</strong>
                      <span>{{$project->bedrooms}}</span>
                    </li>
                    <li>
                      <strong>Bathrooms</strong>
                      <span>{{$project->bathrooms}}</span>
                    </li>
                    <li>
                      <strong>Building Size</strong>
                      <span>{{$project->building}} m<sup>2</sup></span>
                    </li>
                    <li>
                      <strong>Land Size</strong>
                      <span>{{$project->area}} m<sup>2</sup></span>
                    </li>
                    <li>
                      <strong>Garages</strong>
                      <span>{{$project->garages}}</span>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <!-- <div class="single_feature mb-5">
              <h4 class="inner-title mb-4">Features</h4>
              <ul class="icon_list_1">
                <li>Fitness Lab and Room</li>
                <li>Swiming Pools</li>
                <li>Parking Facility</li>
                <li>Green Park View</li>
                <li>Playground Include</li>
                <li>Garden</li>
                <li>Kitchen Furniture</li>
                <li>Fire Security</li>
                <li>High Class Door</li>
                <li>Store Room</li>
                <li>Marble Floor</li>
              </ul>
            </div> -->
          </div>
        </div>
      </div>
      <!-- Property Commsnts -->

      <!-- End Property Comments -->
    </div>
    <div class="col-lg-4">
      <div class="property_sidebar mt_md_50">
        <div class="contact_agent sidebar-widget">
          @if($errors->any())
          <div class="alert alert-danger">{{$errors->first()}}</div>
          @endif
          @if(session()->has('message'))
          <div class="alert alert-success">
            {{ session()->get('message') }}
          </div>
          @endif
          <form action="{{route('contact-us')}}" method="post">
            @csrf
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <input type="text" class="form-control" name="firstname" placeholder="Your Name">
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <input type="text" class="form-control" name="phone" placeholder="Your Phone">
                  <input type="hidden" class="form-control" name="subject" value="property" placeholder="Your Phone">
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <input type="text" class="form-control" name="email" placeholder="Your Email">
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <textarea class="form-control" name="message" placeholder="Message" rows="5"></textarea>
                </div>
              </div>
              <div class="col-md-12">
                <button type="submit" class="btn btn_primary" name="submit">Send</button>
              </div>
            </div>
          </form>
        </div>
        <div class="featured_sidebar sidebar-widget">
          <h4 class="widget-title mb-4">Featured Property</h4>
          <div class="owl-carousel owl_carousel_6">
            @foreach($projects as $val)
            <div class="property_grid_1 property_item">
              <div class="position-relative">
                <div class="zoom_effect_1">
                  <div class="upper_1 bg_secondary text-white">{{$val->status}}</div>
                  <a href="{{url('property?id='.$val->id)}}">
                    <div class="thumb-wrapper">
                      <div class="thumb-image">
                        <img src="{{$val->images[0]->source}}" height="100%" width="auto" alt="Image Not Found!">
                      </div>
                    </div>
                  </a>
                </div>
                <div class="quantity">
                  <ul>
                    <li><span>Land Size</span>{{$val->area}} m<sup>2</sup></li>
                    <li><span>Beds</span>{{$val->bedrooms}}</li>
                    <li><span>Baths</span>{{$val->bathrooms}}</li>
                    <li><span>Garage</span>{{$val->garages}}</li>
                  </ul>
                </div>
              </div>
              <div class="property_text p-3">
                <h5 class="title"><a href="#">{{$val->name}}</a></h5>
                <span class="my-3 d-block"><i class="fas fa-map-marker-alt"></i> {{$val->address}} </span>
              </div>
              <div class="bed_area d-table w-100">
                <ul>
                  <li>Rp {{number_format($val->price)}}</li>
                  <li class="icon_medium"><a href="#"><i class="flaticon-like"></i></a> </li>
                  <li class="icon_medium"><a href="#"><i class="flaticon-connections"></i></a> </li>
                </ul>
              </div>
            </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</section>
@endsection

@section('js')
<script src="front/js/ekko-lightbox.min.js" charset="utf-8"></script>
<script src="front/js/infobox.min.js"></script>
<script type="text/javascript">
  $(document).on("click", '[data-toggle="lightbox"]', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox();
  });
</script>
<script type="text/javascript">
function initialize() {
var myLatlng = new google.maps.LatLng(document.getElementById('lat').value,document.getElementById('lng').value);
var mapProp = {
  center:myLatlng,
  zoom:13,
  mapTypeId:google.maps.MapTypeId.ROADMAP

};
var map=new google.maps.Map(document.getElementById("googleMap"), mapProp);
var marker = new google.maps.Marker({
  position: myLatlng,
  map: map,
  icon: '{{asset("/img/pin.png")}}',
  title: 'Joyglo',
  draggable:false
});

var infobox = new InfoBox({
  disableAutoPan: true, //false
  maxWidth: 250,
  alignBottom: true,
  pixelOffset: new google.maps.Size(-148, -90),
  zIndex: null,
  infoBoxClearance: new google.maps.Size(1, 1),
  isHidden: false,
  pane: "floatPane",
  enableEventPropagation: false,
  boxStyle: {
    width: "300px"
  }
});

google.maps.event.addListener(marker, 'click', function () {
  infobox.setContent(' <div class="marker-content"> <div class="marker-content-item"> <div class="item-thumb"><a href="#"><img width="100" height="100" src="{{$project->images[0]->source}}" alt="{{$project->name}}"></a></div> <div class="item-body"><a href="#" class="title-marker">{{$project->name}}</a> <div class="price-marker">Rp. {{number_format($project->price)}}</div> <div class="address-marker" title="{{$project->address}}"><i class="fa fa-map-marker"></i>{{$project->address}}</div> </div> </div> </div> ');
  infobox.open(map, this);
});

}

google.maps.event.addDomListener(window, 'load', initialize);

</script>
<script type="text/javascript">
$('.owl_carousel_sl').owlCarousel({
  loop: true,
  autoplay: false,
  margin: 30,
  nav: true,
  dots: false,
  navText: ['<i class="fas fa-angle-left"></i>','<i class="fas fa-angle-right"></i>'],
  responsive:{
    0:{
      items:1
    },
    500:{
      items:2
    },
    768:{
      items:3
    },
    1200:{
      items:4
    }
  }
});
</script>

@endsection
