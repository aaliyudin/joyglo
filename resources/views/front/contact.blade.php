@extends('layouts.app')
@section('css')
<link rel="stylesheet" href="front/css/ekko-lightbox.css">
@endsection
@section('content')
<section class="full_row bg_img_5 py_80 overlay_1 page-banner">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page_banner">
                    <h3 class="title text_white d-table float-left">Contact</h3>
                    <ul class="page_location float-right">
                        <li><a href="{{route('index')}}">Home</a> </li>
                        <li><i class="fa fa-angle-right" aria-hidden="true"></i> </li>
                        <li><span>Contact</span> </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Banner Section End -->

<!-- Contact Section Start -->
<section id="contact" class="py_80 bg_gray full_row">
    <div class="container">
        <h2 class="mb-4">Get <span class="text_primary">In Touch</span></h2>
        <div class="row">
            <div class="col-md-6">
                <div class="contact_area">
                  @if($errors->any())
                  <div class="alert alert-danger">{{$errors->first()}}</div>
                  @endif
                  @if(session()->has('message'))
                  <div class="alert alert-success">
                    {{ session()->get('message') }}
                  </div>
                  @endif
                    <form id="contact-form" class="contact_message mt_30" action="{{route('contact-us')}}" method="post">
                      {{ csrf_field() }}
                        <div class="row">
                            <div class="form-group col-md-6 col-sm-6">
                                <input class="form-control" id="firstname" type="text" name="firstname" placeholder="First Name">
                            </div>
                            <div class="form-group col-md-6 col-sm-6">
                                <input class="form-control" id="lastname" type="text" name="lastname" placeholder="Last Name">
                            </div>
                            <div class="form-group col-md-6 col-sm-6">
                                <input class="form-control" id="email" type="text" name="email" placeholder="Email Address">
                            </div>
                            <div class="form-group col-md-6 col-sm-6">
                                <input class="form-control" id="phone" type="text" name="phone" placeholder="Phone">
                            </div>
                            <div class="form-group col-md-6 col-sm-6">
                                <input class="form-control" id="subject" type="text" name="subject" placeholder="Enter Subject">
                            </div>
                            <div class="form-group col-md-12 col-sm-12">
                                <textarea class="form-control" id="message" name="message" placeholder="Message" rows="5"></textarea>
                            </div>
                            <div class="form-group col-md-12 col-sm-12">
                                <input id="send" class="btn btn_primary" type="submit" value="Send">
                            </div>
                            <div class="col-md-12">
                                <div class="error-handel">
                                    <div id="success">Your email sent Successfully, Thank you.</div>
                                    <div id="error"> Error occurred while sending email. Please try again later.</div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-6">
                        <div class="contact_right">
                            <h5 class="mb-4">Our Address</h5>
                            <p>{{$homes->address}}</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="contact_right">
                            <h5 class="mb-4">Contact Info</h5>
                            <p>{{$homes->mobile_phone}}</p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="contact_right">
                            <div id="map"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Contact Section End -->
@endsection

@section('js')
<script src="front/js/ekko-lightbox.min.js" charset="utf-8"></script>
<script type="text/javascript">
  $(document).on("click", '[data-toggle="lightbox"]', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox();
  });
</script>
<script type="text/javascript">
function initialize() {
var myLatlng = new google.maps.LatLng(document.getElementById('lat').value,document.getElementById('lng').value);
var mapProp = {
  center:myLatlng,
  zoom:13,
  mapTypeId:google.maps.MapTypeId.ROADMAP

};
var map=new google.maps.Map(document.getElementById("googleMap"), mapProp);
var marker = new google.maps.Marker({
  position: myLatlng,
  map: map,
  title: 'Hello World!',
  draggable:false
});

}

google.maps.event.addDomListener(window, 'load', initialize);

</script>
<script type="text/javascript">
$('.owl_carousel_sl').owlCarousel({
  loop: true,
  autoplay: false,
  margin: 30,
  nav: true,
  dots: false,
  navText: ['<i class="fas fa-angle-left"></i>','<i class="fas fa-angle-right"></i>'],
  responsive:{
    0:{
      items:1
    },
    500:{
      items:2
    },
    768:{
      items:3
    },
    1200:{
      items:4
    }
  }
});
</script>

@endsection
