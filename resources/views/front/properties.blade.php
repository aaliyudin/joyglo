@extends('layouts.app')
@section('css')
<link rel="stylesheet" href="front/css/ekko-lightbox.css">
@endsection
@section('content')
<section class="full_row bg_img_5 py_80 overlay_1 page-banner">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page_banner">
                    <h3 class="title text_white d-table float-left">Properties Grid</h3>
                    <ul class="page_location float-right">
                        <li><a href="index_1.html">Home</a> </li>
                        <li><i class="fa fa-angle-right" aria-hidden="true"></i> </li>
                        <li><span>Properties Grid</span> </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Banner Section End -->

<!-- Property Grid Start -->
<section class="full_row py_80 bg_gray">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <!-- <div class="row">
                    <div class="col-md-12">
                        <div class="property_sorting mb_30 d-inline-block w-100">
                            <form class="property_filter" action="#" method="post">
                                <select class="selectpicker form-control float-left" data-width="fit">
                                    <option>Any Type</option>
                                    <option>For Rent</option>
                                    <option>For Sale</option>
                                </select>
                                <ul class="float-right">
                                    <li> <span>Order:</span>
                                        <select class="selectpicker form-control" data-width="fit">
                                            <option>Default Order</option>
                                            <option>Featured</option>
                                            <option>Price Hight</option>
                                            <option>Price Low</option>
                                            <option>Latest Item</option>
                                            <option>Oldest Item</option>
                                        </select>
                                    </li>
                                    <li> <a href="property_grid.html"><i class="fa fa-th" aria-hidden="true"></i></a> </li>
                                    <li> <a class="active" href="property_list.html"><i class="fa fa-th-list" aria-hidden="true"></i></a> </li>
                                </ul>
                            </form>
                        </div>
                    </div>
                </div> -->

                <!-- Property Grid Start -->
                <div class="row">
                  @foreach($projectList as $project)
                  <div class="col-md-6">
                      <div class="property_grid_1 property_item bg-white mb_30">
                          <div class="zoom_effect_1">
                              <div class="upper_1 bg_secondary text-white">{{$project->status}}</div>
                              <a href="{{url('property?id='.$project->id)}}">
                                <div class="thumb-wrapper">
                                  <div class="thumb-image">
                                    @if(strlen($project->cover) > 0)
                                    <img src="{{$project->cover}}" height="100%" width="auto" alt="Image Not Found!">
                                    @else
                                    <img src="{{$project->images[0]->source}}" height="100%" width="auto" alt="Image Not Found!">
                                    @endif
                                  </div>
                                </div>
                              </a>
                          </div>
                          <div class="property_text p-3">
                              <h5 class="title"><a href="{{url('property?id='.$project->id)}}">{{$project->name}}</a></h5>
                              <span class="my-3 d-block"><i class="fas fa-map-marker-alt"></i> {{$project->address}} </span>
                              <div class="quantity">
                                  <ul>
                                      <li><span>Area</span>{{$project->area}} M<sup>2</sup></li>
                                      <li><span>Beds</span>{{$project->bedrooms}}</li>
                                      <li><span>Baths</span>{{$project->bathrooms}}</li>
                                      <li><span>Garage</span>{{$project->garages}}</li>
                                  </ul>
                              </div>
                          </div>
                          <div class="bed_area d-table w-100">
                              <ul>
                                  <li>Rp {{number_format($project->price)}}</li>
                              </ul>
                          </div>
                      </div>
                  </div>
                  @endforeach
                </div>
                {{ $projectList->links() }}
                <!-- End Property Grid -->

                <!-- <div class="row">
                    <div class="col-md-12">
                        <div class="pagination_style1">
                            <nav aria-label="page navigation">
                                <ul class="pagination">
                                    <li><a href="#" aria-label="Previous"> <span aria-hidden="true">Prev</span> </a> </li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li> <a href="#" aria-label="Next"> <span aria-hidden="true">Next</span> </a> </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div> -->
            </div>
            <div class="col-lg-4">
              <div class="property_sidebar mt_md_50">
                <div class="contact_agent sidebar-widget">
                  @if($errors->any())
                  <div class="alert alert-danger">{{$errors->first()}}</div>
                  @endif
                  @if(session()->has('message'))
                  <div class="alert alert-success">
                    {{ session()->get('message') }}
                  </div>
                  @endif
                  <form action="{{route('contact-us')}}" method="post">
                    @csrf
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <input type="text" class="form-control" name="firstname" placeholder="Your Name">
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <input type="text" class="form-control" name="phone" placeholder="Your Phone">
                          <input type="hidden" class="form-control" name="subject" value="property" placeholder="Your Phone">
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <input type="text" class="form-control" name="email" placeholder="Your Email">
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <textarea class="form-control" name="message" placeholder="Message" rows="5"></textarea>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <button type="submit" class="btn btn_primary" name="submit">Send</button>
                      </div>
                    </div>
                  </form>
                </div>
                <div class="featured_sidebar sidebar-widget">
                  <h4 class="widget-title mb-4">Featured Property</h4>
                  <div class="owl-carousel owl_carousel_6">
                    @foreach($projects as $val)
                    <div class="property_grid_1 property_item">
                      <div class="position-relative">
                        <div class="zoom_effect_1">
                          <div class="upper_1 bg_secondary text-white">{{$val->status}}</div>
                          <a href="{{url('property?id='.$val->id)}}">
                            <div class="thumb-wrapper">
                              <div class="thumb-image">
                                @if(strlen($val->cover) > 0)
                                <img src="{{$val->cover}}" height="100%" width="auto" alt="Image Not Found!">
                                @else
                                <img src="{{$val->images[0]->source}}" height="100%" width="auto" alt="Image Not Found!">
                                @endif
                              </div>
                            </div>
                          </a>
                        </div>
                        <div class="quantity">
                          <ul>
                            <li><span>Area</span>{{$val->area}} m<sup>2</sup></li>
                            <li><span>Beds</span>{{$val->bedrooms}}</li>
                            <li><span>Baths</span>{{$val->bathrooms}}</li>
                            <li><span>Garage</span>{{$val->garages}}</li>
                          </ul>
                        </div>
                      </div>
                      <div class="property_text p-3">
                        <h5 class="title"><a href="#">{{$val->name}}</a></h5>
                        <span class="my-3 d-block"><i class="fas fa-map-marker-alt"></i> {{$val->address}} </span>
                      </div>
                      <div class="bed_area d-table w-100">
                        <ul>
                          <li>Rp {{number_format($val->price)}}</li>
                          <li class="icon_medium"><a href="#"><i class="flaticon-like"></i></a> </li>
                          <li class="icon_medium"><a href="#"><i class="flaticon-connections"></i></a> </li>
                        </ul>
                      </div>
                    </div>
                    @endforeach
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
</section>
<!-- Property Grid End -->
@endsection

@section('js')
<script src="front/js/ekko-lightbox.min.js" charset="utf-8"></script>
<script type="text/javascript">
  $(document).on("click", '[data-toggle="lightbox"]', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox();
  });
</script>
<script type="text/javascript">
function initialize() {
var myLatlng = new google.maps.LatLng(document.getElementById('lat').value,document.getElementById('lng').value);
var mapProp = {
  center:myLatlng,
  zoom:13,
  mapTypeId:google.maps.MapTypeId.ROADMAP

};
var map=new google.maps.Map(document.getElementById("googleMap"), mapProp);
var marker = new google.maps.Marker({
  position: myLatlng,
  map: map,
  title: 'Hello World!',
  draggable:false
});

}

google.maps.event.addDomListener(window, 'load', initialize);

</script>
<script type="text/javascript">
$('.owl_carousel_sl').owlCarousel({
  loop: true,
  autoplay: false,
  margin: 30,
  nav: true,
  dots: false,
  navText: ['<i class="fas fa-angle-left"></i>','<i class="fas fa-angle-right"></i>'],
  responsive:{
    0:{
      items:1
    },
    500:{
      items:2
    },
    768:{
      items:3
    },
    1200:{
      items:4
    }
  }
});
</script>

@endsection
