@extends('layouts.app')
@section('css')
<link rel="stylesheet" href="front/css/ekko-lightbox.css">
@endsection
@section('content')
<!-- Banner Section Start -->
<section class="full_row bg_img_5 py_80 overlay_1 page-banner">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page_banner">
                    <h3 class="title text_white d-table float-left">Blog Details</h3>
                    <ul class="page_location float-right">
                        <li><a href="{{route('index')}}">Home</a> </li>
                        <li><i class="fa fa-angle-right" aria-hidden="true"></i> </li>
                        <li><a href="#">Blog</a> </li>
                        <li><i class="fa fa-angle-right" aria-hidden="true"></i> </li>
                        <li><span>Blog Details</span> </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Banner Section End -->

<!-- Blog List Start -->
<section class="full_row py_80 bg_gray">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="row">
                    <div class="col-md-12"> <img src="{{$blog->images}}" alt="Image not found!"> </div>
                    <div class="col-md-12">
                        <div class="single_blog_detail p_20 bg_white">
                            <div class="head mb-4">
                                <h4 class="title mb-2">{{$blog->title}}</h4>
                                <div class="post-info"><span>posted by <a href="#">Joyglo</a> On {{$blog->created_at}}</div>
                            </div>
                            <div class="content-blog">
                              {!!$blog->content!!}
                            </div>
                            <!-- <div class="socail_media_1 my-5 type_2">
                                <ul>
                                    <li><span>share this post in your media: </span></li>
                                    <li><a href="https://www.facebook.com"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="https://twitter.com"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="https://www.linkedin.com"><i class="fab fa-linkedin-in"></i></a></li>
                                    <li><a href="https://plus.google.com"><i class="fab fa-google-plus-g"></i></a></li>
                                    <li><a href="https://vimeo.com"><i class="fab fa-vimeo-v"></i></a></li>
                                </ul>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
              <div class="property_sidebar mt_md_50">
                <div class="contact_agent sidebar-widget">
                  @if($errors->any())
                  <div class="alert alert-danger">{{$errors->first()}}</div>
                  @endif
                  @if(session()->has('message'))
                  <div class="alert alert-success">
                    {{ session()->get('message') }}
                  </div>
                  @endif
                  <form action="{{route('contact-us')}}" method="post">
                    @csrf
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <input type="text" class="form-control" name="firstname" placeholder="Your Name">
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <input type="text" class="form-control" name="phone" placeholder="Your Phone">
                          <input type="hidden" class="form-control" name="subject" value="blog" placeholder="Your Phone">
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <input type="text" class="form-control" name="email" placeholder="Your Email">
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <textarea class="form-control" name="message" placeholder="Message" rows="5"></textarea>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <button type="submit" class="btn btn_primary" name="submit">Send</button>
                      </div>
                    </div>
                  </form>
                </div>
                <div class="featured_sidebar sidebar-widget">
                  <h4 class="widget-title mb-4">Featured Property</h4>
                  <div class="owl-carousel owl_carousel_6">
                    @foreach($projects as $val)
                    <div class="property_grid_1 property_item">
                      <div class="position-relative">
                        <div class="zoom_effect_1">
                          <div class="upper_1 bg_secondary text-white">{{$val->status}}</div>
                          <a href="{{url('property?id='.$val->id)}}">
                            <div class="thumb-wrapper">
                              <div class="thumb-image">
                                <img src="{{$val->images[0]->source}}" height="100%" width="auto" alt="Image Not Found!">
                              </div>
                            </div>
                          </a>
                        </div>
                        <div class="quantity">
                          <ul>
                            <li><span>Land Size</span>{{$val->area}} m<sup>2</sup></li>
                            <li><span>Beds</span>{{$val->bedrooms}}</li>
                            <li><span>Baths</span>{{$val->bathrooms}}</li>
                            <li><span>Garage</span>{{$val->garages}}</li>
                          </ul>
                        </div>
                      </div>
                      <div class="property_text p-3">
                        <h5 class="title"><a href="#">{{$val->name}}</a></h5>
                        <span class="my-3 d-block"><i class="fas fa-map-marker-alt"></i> {{$val->address}} </span>
                      </div>
                      <div class="bed_area d-table w-100">
                        <ul>
                          <li>Rp {{number_format($val->price)}}</li>
                          <li class="icon_medium"><a href="#"><i class="flaticon-like"></i></a> </li>
                          <li class="icon_medium"><a href="#"><i class="flaticon-connections"></i></a> </li>
                        </ul>
                      </div>
                    </div>
                    @endforeach
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
<script src="front/js/ekko-lightbox.min.js" charset="utf-8"></script>
<script type="text/javascript">
  $(document).on("click", '[data-toggle="lightbox"]', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox();
  });
</script>
<script type="text/javascript">
function initialize() {
var myLatlng = new google.maps.LatLng(document.getElementById('lat').value,document.getElementById('lng').value);
var mapProp = {
  center:myLatlng,
  zoom:13,
  mapTypeId:google.maps.MapTypeId.ROADMAP

};
var map=new google.maps.Map(document.getElementById("googleMap"), mapProp);
var marker = new google.maps.Marker({
  position: myLatlng,
  map: map,
  title: 'Hello World!',
  draggable:false
});

}

google.maps.event.addDomListener(window, 'load', initialize);

</script>
<script type="text/javascript">
$('.owl_carousel_sl').owlCarousel({
  loop: true,
  autoplay: false,
  margin: 30,
  nav: true,
  dots: false,
  navText: ['<i class="fas fa-angle-left"></i>','<i class="fas fa-angle-right"></i>'],
  responsive:{
    0:{
      items:1
    },
    500:{
      items:2
    },
    768:{
      items:3
    },
    1200:{
      items:4
    }
  }
});
</script>

@endsection
