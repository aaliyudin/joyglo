@extends('layouts.dashboard')

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Inbox</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item"><a href="{{route('dashboard-mailbox')}}">Inbox</a></li>
            <li class="breadcrumb-item active">View</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- /.col -->
      <div class="col-md-12">
        <div class="card card-primary card-outline">
          <div class="card-header">
            <h3 class="card-title">Read Mail</h3>
            <!-- <div class="card-tools">
              <a href="#" class="btn btn-tool" data-toggle="tooltip" title="Previous">
                <i class="fas fa-chevron-left"></i>
              </a>
              <a href="#" class="btn btn-tool" data-toggle="tooltip" title="Next">
                <i class="fas fa-chevron-right"></i>
              </a>
            </div> -->
          </div>
          <!-- /.card-header -->
          <div class="card-body p-0">
            <div class="mailbox-read-info">
              <h5>{{$mails->subject}}</h5>
              <h6>From: {{$mails->email}} | {{$mails->phone}}
                <span class="mailbox-read-time float-right">{{$mails->created_at}}</span>
              </h6>
            </div>
            <!-- /.mailbox-read-info -->
            <!-- /.mailbox-controls -->
            <div class="mailbox-read-message">
              <p>{{$mails->message}}</p>
            </div>
            <!-- /.mailbox-read-message -->
          </div>
          <!-- /.card-body -->
          <div class="card-footer">
            <!-- <div class="float-right">
              <button type="button" class="btn btn-default">
                <i class="fas fa-reply"></i> Reply
              </button>
              <button type="button" class="btn btn-default">
                <i class="fas fa-share"></i> Forward
              </button>
            </div>
            <button type="button" class="btn btn-default">
              <i class="far fa-trash-alt"></i> Delete
            </button>
            <button type="button" class="btn btn-default">
              <i class="fas fa-print"></i> Print
            </button> -->
          </div>
          <!-- /.card-footer -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>

<div class="modal fade" id="modal-sm">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Delete Project</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Are you sure to delete this?</p>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="delete-project" data-id="0">Delete</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
@endsection
