@extends('layouts.dashboard')

@section('css')
<style>
  .dataTables_length {
    margin-left: 10px;
    margin-top: 10px;
  }
  div.dataTables_wrapper div.dataTables_info {
    padding: 10px;
  }
  div.dataTables_wrapper div.dataTables_filter {
    padding: 10px;
  }
</style>
@endsection

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Inbox</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Inbox</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- /.col -->
      <div class="col-md-12">
        <div class="card card-primary card-outline">
          <div class="card-header">
            <h3 class="card-title">Inbox</h3>
            <!-- /.card-tools -->
          </div>
          <!-- /.card-header -->
          <div class="card-body p-0">
            <div class="table-responsive mailbox-messages">
              <table id="mail-table" class="table table-hover table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Subject</th>
                  <th>Phone</th>
                  <th>Message</th>
                  <th>Date</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                </tbody>
              </table>
              <!-- /.table -->
            </div>
            <!-- /.mail-box-messages -->
          </div>
          <!-- /.card-body -->
          <div class="card-footer p-0">
          </div>
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>

<div class="modal fade" id="modal-sm">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Delete</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Are you sure to delete this?</p>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="delete-project" data-id="0">Delete</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
@endsection

@section('js')
<script>
function timeSince(date) {

  var seconds = Math.floor((new Date() - date) / 1000);

  var interval = Math.floor(seconds / 31536000);

  if (interval > 1) {
    return interval + " years ago";
  }
  interval = Math.floor(seconds / 2592000);
  if (interval > 1) {
    return interval + " months ago";
  }
  interval = Math.floor(seconds / 86400);
  if (interval > 1) {
    return interval + " days ago";
  }
  interval = Math.floor(seconds / 3600);
  if (interval > 1) {
    return interval + " hours ago";
  }
  interval = Math.floor(seconds / 60);
  if (interval > 1) {
    return interval + " minutes ago";
  }
  return Math.floor(seconds) + " seconds ago";
}
$(function() {
    $('#mail-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{route('dashboard-mailbox-list')}}',
        columns: [
          { data: 'name', name: 'name' },
          { data: 'subject', name: 'subject' },
          { data: 'phone', name: 'phone' },
          {
            "data": null,
            "sortable": false,
            "render": function ( data, type, full, meta ) {
              var messageId = "{{route('dashboard-mailbox-view')}}?id="+full.id;
              var message = full.message;
              function truncate(str, n){
                return (str.length > n) ? str.substr(0, n-1) + '&hellip;' : str;
              };
              return "<a class='' href='"+messageId+"'>"+
              truncate(message, 50)+
              "</a>"
            }
          },
          {
            "data": 'created_at',
            "sortable": true,
            "render": function ( data, type, full, meta ) {
              var date = new Date(full.created_at);

              return timeSince(date);
            }
          },
          {
            "data": null,
            "sortable": false,
            "render": function ( data, type, full, meta ) {
                   var buttonID = "{{route('dashboard-mailbox-view')}}?id="+full.id;
                   return "<a class='btn btn-primary btn-sm' href='"+buttonID+"'><i class='fas fa-eye'></i></a>"+
                   "<button class='btn btn-danger btn-sm delete-id' data-id='"+full.id+"' href='#'>"+
                     "<i class='fas fa-trash'>"+
                     "</i>"+
                   "</button>"
               }
          }
        ],
        initComplete : function( settings, json ) {
          $('.delete-id').on('click', function(){
            var id = $(this).attr('data-id');
            $('#delete-project').attr('data-id', id);
            $('#modal-sm').modal();
          });

          $('#delete-project').on('click', function(){
            var id = $(this).attr('data-id');
            var data = {
              '_token': '{{ csrf_token() }}',
              'id'    : id
            };
            var url = '{{route('dashboard-mailbox-delete')}}';
            $.ajax(
              {
                type    : 'POST',
                url     : url,
                data    : data,
                success : function(response)
                {
                  $('#modal-sm').modal('hide');
                  var table = $('#mail-table').DataTable();
                  table.ajax.reload();
                }
              });
          });
        }
    });
});
</script>
@endsection
