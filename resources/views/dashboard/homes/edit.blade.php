@extends('layouts.dashboard')

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Project Add</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item active"><a href="{{route('dashboard-projects')}}">Projects</a></li>
            <li class="breadcrumb-item active">Edit</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <form class="" action="{{route('dashboard-projects-update')}}" method="post" enctype="multipart/form-data">
      @csrf
      <div class="row">
        @if ($errors->any())
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
        @endif
        <div class="col-md-6">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">General</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fas fa-minus"></i></button>
                </div>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label for="name">Project Name</label>
                  <input type="text" id="name" name="name" value="{{$data->name}}" class="form-control">
                </div>
                <div id="googleMap" style="height: 400px;"></div>
                <input type='hidden' value="{{$data->latitude}}" name='latitude' id='lat'>
                <input type='hidden' value="{{$data->longitude}}" name='longitude' id='lng'>
                <div class="form-group">
                  <label for="address">Project Address</label>
                  <textarea id="address" name="address" class="form-control" rows="3">{{$data->address}}</textarea>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-6">
                      <label for="country">Country</label>
                      <select id="country" name="country" class="form-control custom-select">
                        <option selected disabled>Select one</option>
                        <option>Sale</option>
                        <option>Rent</option>
                      </select>
                    </div>
                    <div class="col-md-6">
                      <label for="city">City</label>
                      <select id="city" name="city" class="form-control custom-select">
                        <option selected disabled>Select one</option>
                        <option>Sale</option>
                        <option>Rent</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="description">Project Description</label>
                  <textarea id="description" name="description" class="form-control" rows="4"></textarea>
                </div>
                <div class="form-group">
                  <label for="status">Property Status</label>
                  <select id="status" name="status" class="form-control custom-select">
                    <option selected disabled>Select one</option>
                    <option>Sale</option>
                    <option>Rent</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="type">Property Type</label>
                  <select id="type" name="type" class="form-control custom-select">
                    <option selected disabled>Select one</option>
                    <option>House</option>
                    <option>Apartment</option>
                  </select>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <div class="col-md-6">
            <div class="card card-secondary">
              <div class="card-header">
                <h3 class="card-title">Detail</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                  </div>
                </div>
                <div class="card-body">
                  <div class="form-group">
                      <label class="active">Photos</label>
                      <div class="input-images-1" style="padding-top: .5rem;"></div>
                      <?php $i = 0 ?>
                      @foreach($data->images as $image)
                      <input type="hidden" class="input_image" name="input_image_{{$i}}" data-id="{{$image->id}}" value="{{$image->source}}">
                      <?php $i++ ?>
                      @endforeach
                  </div>
                  <div class="form-group">
                    <label for="inputStatus">Property Price</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Rp</span>
                      </div>
                      <input type="text" name="price" class="form-control">
                      <div class="input-group-append">
                        <span class="input-group-text">.00</span>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-md-6">
                        <label for="inputBedroom">Bedroom(s)</label>
                        <input type="number" name="bedrooms" class="form-control" name="" value="">
                      </div>
                      <div class="col-md-6">
                        <label for="inputBathroom">Bathroom(s)</label>
                        <input type="number" name="bathrooms" class="form-control" name="" value="">
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-md-6">
                        <label for="inputLand">Land Area</label>
                        <div class="input-group">
                          <input type="number" name="area" class="form-control" name="" value="">
                          <div class="input-group-append">
                            <span class="input-group-text">m<sup>2</sup></span>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <label for="inputLand">Building Area</label>
                        <div class="input-group">
                          <input type="number" name="building" class="form-control" name="" value="">
                          <div class="input-group-append">
                            <span class="input-group-text">m<sup>2</sup></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-md-6">
                        <label for="garages">Garages / Carpot</label>
                        <div class="input-group">
                          <input type="number" name="garages" class="form-control" name="" value="">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-md-6">
                        <label for="inputYear">Year Built</label>
                        <div class="input-group">
                          <input type="number" name="year" class="form-control" name="" value="">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
              </div>
              <div class="card card-success">
                <div class="card-header">
                  <h3 class="card-title">Availability</h3>
                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fas fa-minus"></i></button>
                  </div>
                </div>
                <div class="card-body">
                  <div class="form-group">
                    <div class="row">
                      <div class="col-md-6">
                        <label for="inputAvailable">Unit Available</label>
                        <div class="input-group">
                          <input type="number" name="available" class="form-control" name="" value="">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-md-6">
                        <label for="is_active">Is Active</label>
                        <div class="input-group">
                          <input type="checkbox" id="is_active" name="is_active" checked data-bootstrap-switch data-off-color="danger" data-on-color="success">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <label for="is_slideshow">Is Slideshow</label>
                        <div class="input-group">
                          <input type="checkbox" id="is_slideshow" name="is_slideshow" checked data-bootstrap-switch data-off-color="danger" data-on-color="success">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card -->
            </div>
          </div>
          <div class="row">
            <div class="col-12">
              <a href="#" class="btn btn-secondary">Cancel</a>
              <input type="submit" value="Create new Porject" class="btn btn-success float-right">
            </div>
          </div>
        </form>
      </section>
      <br>
      <!-- /.content -->
    </div>

    @endsection

    @section('js')
    <script type="text/javascript">
    function initialize() {
    var myLatlng = new google.maps.LatLng(document.getElementById('lat').value,document.getElementById('lng').value);
    var mapProp = {
      center:myLatlng,
      zoom:13,
      mapTypeId:google.maps.MapTypeId.ROADMAP

    };
    var map=new google.maps.Map(document.getElementById("googleMap"), mapProp);
    var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      title: 'Hello World!',
      draggable:true
    });
    document.getElementById('lat').value= -6.229728
    document.getElementById('lng').value= 106.6894284
    // marker drag event
    google.maps.event.addListener(marker,'drag',function(event) {
      document.getElementById('lat').value = event.latLng.lat();
      document.getElementById('lng').value = event.latLng.lng();
    });

    //marker drag event end
    google.maps.event.addListener(marker,'dragend',function(event) {
      document.getElementById('lat').value = event.latLng.lat();
      document.getElementById('lng').value = event.latLng.lng();
      alert("lat=>"+event.latLng.lat());
      alert("long=>"+event.latLng.lng());
      });
    }

    google.maps.event.addDomListener(window, 'load', initialize);

    </script>
    <script type="text/javascript">
    var c_input_img = $('.input_image').length;
    var imgs = new Array();
    for (var i = 0; i < c_input_img; i++) {
      imgs[i]= new Array();
      imgs[i]['src'] = $("input[name=input_image_"+i+"]").val();
      imgs[i]['id'] = $("input[name=input_image_"+i+"]").attr('data-id');
    }
    let preloaded = imgs;

    $('.input-images-1').imageUploader({
      preloaded: preloaded
    });

    $('.uploaded-image button').hide();

    $('.uploaded-image').on('click', function(){
      $(this).find('button').trigger('click');
      var inpv = $(this).find('input').val();
      alert(inpv);
    });
    </script>
    <script type="text/javascript">
      $("input[data-bootstrap-switch]").each(function(){
        $(this).bootstrapSwitch('state', $(this).prop('checked'));
      });
    </script>
    @endsection
