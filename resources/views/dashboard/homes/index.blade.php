@extends('layouts.dashboard')
@section('css')
<style media="screen">
.image-uploader .uploaded .uploaded-image .delete-image {
  display: block !important;
}

#pac-input {
  background-color: #fff;
  font-family: Roboto;
  font-size: 15px;
  font-weight: 300;
  margin-left: 12px;
  padding: 0 11px 0 13px;
  text-overflow: ellipsis;
  width: 400px;
  top: 55px !important;
  left: 35px !important;
}

#pac-input:focus {
  border-color: #4d90fe;
}
</style>
@endsection
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Finance Add</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item active">Home Setting</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <form class="" action="{{route('dashboard-home-setting-updateinput')}}" method="post" enctype="multipart/form-data">
      @csrf
      @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif
      <div class="row">
        <div class="col-md-12"><div class="card card-secondary">
          <div class="card-header">
            <h3 class="card-title">Setting</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
              </div>
            </div>
            <div class="card-body">
              <div class="form-group">
                <label for="name">Address</label>
                <input type="text" id="address" name="address" value="{{$homes->address}}" class="form-control">
              </div>
              <div class="form-group">
                <label for="name">Email</label>
                <input type="email" id="email" name="email" value="{{$homes->email}}" class="form-control">
              </div>
              <div class="form-group">
                <label for="inputImage">Background Home</label>
                <input type="file" id="background_home" name="background_home" accept=".jpg,.jpeg,.png,.gif,.bmp,.tiff">
                <br>
                <img id="image_upload_preview_1" src="{{$homes->background_home}}" alt="your image" height="200"/>
              </div>
              <div class="form-group d-none">
                <label for="inputImage">Background Single</label>
                <input type="file" id="background_other" name="background_other" accept=".jpg,.jpeg,.png,.gif,.bmp,.tiff">
                <br>
                <img id="image_upload_preview_2" src="{{$homes->background_other}}" alt="your image" height="200"/>
              </div>
              <div class="form-group">
                <label for="name">Facebook</label>
                <input type="text" id="social_facebook" name="social_facebook" value="{{$homes->social_facebook}}" class="form-control">
              </div>
              <div class="form-group">
                <label for="name">Twitter</label>
                <input type="text" id="social_twitter" name="social_twitter" value="{{$homes->social_twitter}}" class="form-control">
              </div>
              <div class="form-group">
                <label for="name">Whatsapp / Mobile Number</label>
                <input type="text" placeholder="628xxxx" id="mobile_phone" name="mobile_phone" value="{{$homes->mobile_phone}}" class="form-control">
              </div>
              <div class="form-group">
                <label for="name">Whatsapp Message</label>
                <textarea type="text" placeholder="Message" id="message_wa" name="message_wa" value="{{$homes->message_wa}}" class="form-control">{{$homes->message_wa}}</textarea>
              </div>
              <div class="form-group">
                <label for="name">Json IG</label>
                <textarea type="text" placeholder="Message" id="json_code" name="json_code" value="{{$ig}}" class="form-control">{{$ig}}</textarea>
                @foreach($ig as $insta)
                <img src="{{$insta->thumbnail_src}}" alt="Joylo.id" width="50">
                @endforeach
                <br>
                Jika thumbnail Instagram tidak update, silahkan klik link ini <a href="https://www.instagram.com/joyglo.idn/?__a=1" target="_blank">https://www.instagram.com/joyglo.idn/?__a=1</a>
                <br>
                Lalu copy semua dan paste ke kotak diatas
              </div>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <a href="{{route('dashboard')}}" class="btn btn-secondary">Cancel</a>
          <input type="submit" value="Submit" class="btn btn-success float-right">
        </div>
      </div>
    </form>
  </section>
  <br>
  <!-- /.content -->
</div>

@endsection

@section('js')
<script type="text/javascript">
  function readURL1(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#image_upload_preview_1').attr('src', e.target.result);
      }

      reader.readAsDataURL(input.files[0]);
    }
  }

  $("#background_home").change(function () {
    readURL1(this);
  });

  function readURL2(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#image_upload_preview_2').attr('src', e.target.result);
      }

      reader.readAsDataURL(input.files[0]);
    }
  }

  $("#background_other").change(function () {
    readURL2(this);
  });
</script>
<script type="text/javascript">
  var c_input_img = $('.input_image').length;
  var imgs = new Array();
  for (var i = 0; i < c_input_img; i++) {
    imgs[i]= new Array();
    imgs[i]['src'] = $("input[name=input_image_"+i+"]").val();
    imgs[i]['id'] = $("input[name=input_image_"+i+"]").attr('data-id');
  }
  let preloaded = imgs;

  $('.input-images-1').imageUploader({
    preloaded: preloaded
  });

  $('.delete-image').on('click', function(){
    var id = $(this).nextAll("input").val();
    var data = {
      '_token': '{{ csrf_token() }}',
      'id'    : id
    };
    var url = '{{route('dashboard-financing-image-erase')}}';
    $.ajax(
    {
      type    : 'POST',
      url     : url,
      data    : data,
      success : function(response)
      {
        $('#modal-default').modal('hide');
      }
    });
  })

</script>
<script type="text/javascript">
  $("input[data-bootstrap-switch]").each(function(){
    $(this).bootstrapSwitch('state', $(this).prop('checked'));
  });
</script>
@endsection
