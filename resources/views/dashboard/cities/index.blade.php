@extends('layouts.dashboard')

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Cities</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Cities</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <section class="content">

    <!-- Default box -->
    <div class="card">
      <div class="card-header">
        <div class="row">
          <div class="col-md-6">
            <h3 class="card-title">Cities List</h3>
          </div>
          <div class="col-md-6">
            <a class="btn btn-success btn-sm float-right" href="{{route('dashboard-cities-add')}}">
              <i class="fas fa-plus">
              </i>
              Add
            </a>
          </div>
        </div>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <table id="city-table" class="table table-bordered table-striped">
          <thead>
          <tr>
            <th>Citie Name</th>
            <th>Action</th>
          </tr>
          </thead>
          <tbody>
          <tr>
            <td>Joyglo Tebet</td>
            <td class="project-actions text-right">
              <a class="btn btn-primary btn-sm" href="#">
                <i class="fas fa-eye">
                </i>
              </a>
              <a class="btn btn-info btn-sm" href="#">
                <i class="fas fa-pencil-alt">
                </i>
              </a>
              <a class="btn btn-danger btn-sm" href="#">
                <i class="fas fa-trash">
                </i>
              </a>
            </td>
          </tr>
          </tbody>
          <tfoot>
          <tr>
            <th>Cities Name</th>
            <th>Action</th>
          </tr>
          </tfoot>
        </table>
      </div>
      <!-- /.card-body -->
    </div>

  </section>
</div>

@endsection

@section('js')
<script>
$(function() {
    $('#city-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{route('dashboard-cities-list')}}',
        columns: [
            { data: 'name', name: 'name' },
            {
              "data": null,
              "sortable": false,
              "render": function ( data, type, full, meta ) {
                     var buttonID = "{{route('dashboard-cities-edit')}}?id="+full.id;
                     return "<a class='btn btn-info btn-sm' href='"+buttonID+"'>"+
                       "<i class='fas fa-pencil-alt'>"+
                       "</i>"+
                     "</a>"+
                     "<a class='btn btn-danger btn-sm' href='#'>"+
                       "<i class='fas fa-trash'>"+
                       "</i>"+
                     "</a>"
                 }
            }
        ]
    });
});
</script>
@endsection
