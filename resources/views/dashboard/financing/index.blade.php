@extends('layouts.dashboard')
@section('css')
<style media="screen">
.image-uploader .uploaded .uploaded-image .delete-image {
  display: block !important;
}

#pac-input {
  background-color: #fff;
  font-family: Roboto;
  font-size: 15px;
  font-weight: 300;
  margin-left: 12px;
  padding: 0 11px 0 13px;
  text-overflow: ellipsis;
  width: 400px;
  top: 55px !important;
  left: 35px !important;
}

#pac-input:focus {
  border-color: #4d90fe;
}
</style>
@endsection
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Finance Add</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item active"><a href="{{route('dashboard-projects')}}">Finances</a></li>
            <li class="breadcrumb-item active">Edit</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <form class="" action="{{route('dashboard-financing-input')}}" method="post" enctype="multipart/form-data">
      @csrf
      <div class="row">
        <div class="col-md-6"><div class="card card-secondary">
          <div class="card-header">
            <h3 class="card-title">Images</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
              </div>
            </div>
            <div class="card-body">
              <div class="form-group">
                <label class="active">Photos</label>
                <div class="input-images-1" style="padding-top: .5rem;"></div>
                <?php $i = 0 ?>
                @foreach($financings as $financing)
                <input type="hidden" class="input_image" name="input_image_{{$i}}" data-id="{{$financing->id}}" value="{{$financing->src}}">
                <?php $i++ ?>
                @endforeach
              </div>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <a href="{{route('dashboard')}}" class="btn btn-secondary">Cancel</a>
          <input type="submit" value="Submit" class="btn btn-success float-right">
        </div>
      </div>
    </form>
  </section>
  <br>
  <!-- /.content -->
</div>

@endsection

@section('js')
<script type="text/javascript">
  var c_input_img = $('.input_image').length;
  var imgs = new Array();
  for (var i = 0; i < c_input_img; i++) {
    imgs[i]= new Array();
    imgs[i]['src'] = $("input[name=input_image_"+i+"]").val();
    imgs[i]['id'] = $("input[name=input_image_"+i+"]").attr('data-id');
  }
  let preloaded = imgs;

  $('.input-images-1').imageUploader({
    preloaded: preloaded
  });

  $('.delete-image').on('click', function(){
    var id = $(this).nextAll("input").val();
    var data = {
      '_token': '{{ csrf_token() }}',
      'id'    : id
    };
    var url = '{{route('dashboard-financing-image-erase')}}';
    $.ajax(
    {
      type    : 'POST',
      url     : url,
      data    : data,
      success : function(response)
      {
        $('#modal-default').modal('hide');
      }
    });
  })

</script>
<script type="text/javascript">
  $("input[data-bootstrap-switch]").each(function(){
    $(this).bootstrapSwitch('state', $(this).prop('checked'));
  });
</script>
@endsection
