@extends('layouts.dashboard')
@section('css')
  <style media="screen">
  #pac-input {
      background-color: #fff;
      font-family: Roboto;
      font-size: 15px;
      font-weight: 300;
      margin-left: 12px;
      padding: 0 11px 0 13px;
      text-overflow: ellipsis;
      width: 400px;
      top: 55px !important;
      left: 35px !important;
    }

    #pac-input:focus {
      border-color: #4d90fe;
    }
  </style>
@endsection
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Project Add</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item active"><a href="{{route('dashboard-projects')}}">Projects</a></li>
            <li class="breadcrumb-item active">Add</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <form class="" action="{{route('dashboard-projects-input')}}" method="post" enctype="multipart/form-data">
      @csrf
      <div class="row">
        <div class="col-md-6">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">General</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fas fa-minus"></i></button>
                </div>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label for="name">Project Name</label>
                  <input required type="text" id="name" name="name" class="form-control">
                </div>
                <div id="googleMap" style="height: 400px;"></div>
                <input id="pac-input" class="controls" type="text" placeholder="Search Box">
                <input required type='hidden' name='latitude' id='lat'>
                <input required type='hidden' name='longitude' id='lng'>
                <div class="form-group">
                  <label for="address">Project Address</label>
                  <textarea id="address" name="address" class="form-control" rows="3"></textarea>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-6">
                      <label for="country">Country</label>
                      <select id="country" name="country" class="form-control custom-select">
                        <option selected disabled>Select one</option>
                        @foreach($country as $c)
                        <option value="{{$c->name}}">{{$c->name}}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="col-md-6">
                      <label for="city">City</label>
                      <select id="city" name="city" class="form-control custom-select">
                        <option selected disabled>Select one</option>
                        @foreach($city as $c)
                        <option value="{{$c->name}}">{{$c->name}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="description">Project Description</label>
                  <textarea id="description" name="description" class="form-control textarea" rows="4"></textarea>
                </div>
                <div class="form-group">
                  <label for="status">Property Status</label>
                  <select id="status" name="status" class="form-control custom-select">
                    <option selected disabled>Select one</option>
                    <option>Sale</option>
                    <option>Rent</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="type">Property Type</label>
                  <select id="type" name="type" class="form-control custom-select">
                    <option selected disabled>Select one</option>
                    <option>House</option>
                    <option>Apartment</option>
                  </select>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <div class="col-md-6">
            <div class="card card-secondary">
              <div class="card-header">
                <h3 class="card-title">Detail</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                  </div>
                </div>
                <div class="card-body">
                  <div class="form-group">
                    <label for="inputImage">Cover Image</label>
                    <input type="file" id="cover" name="cover" accept=".jpg,.jpeg,.png,.gif,.bmp,.tiff">
                    <br>
                    <img id="image_upload_preview" src="http://placehold.it/516x516" alt="your image" height="200"/>
                  </div>
                  <div class="form-group">
                    <label class="active">Photos</label>
                    <div class="input-images-1" style="padding-top: .5rem;"></div>
                  </div>
                  <div class="form-group">
                    <label for="inputStatus">Property Price</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Rp</span>
                      </div>
                      <input required type="text" name="price" class="form-control">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-md-6">
                        <label for="inputBedroom">Bedroom(s)</label>
                        <input required type="text" name="bedrooms" class="form-control" name="" value="">
                      </div>
                      <div class="col-md-6">
                        <label for="inputBathroom">Bathroom(s)</label>
                        <input required type="text" name="bathrooms" class="form-control" name="" value="">
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-md-6">
                        <label for="inputLand">Land Area</label>
                        <div class="input-group">
                          <input required type="number" name="area" class="form-control" name="" value="">
                          <div class="input-group-append">
                            <span class="input-group-text">m<sup>2</sup></span>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <label for="inputLand">Building Area</label>
                        <div class="input-group">
                          <input required type="number" name="building" class="form-control" name="" value="">
                          <div class="input-group-append">
                            <span class="input-group-text">m<sup>2</sup></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-md-6">
                        <label for="garages">Garages / Carpot</label>
                        <div class="input-group">
                          <input required type="number" name="garages" class="form-control" name="" value="">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-md-6">
                        <label for="inputYear">Year Built</label>
                        <div class="input-group">
                          <input required type="number" name="year" class="form-control" name="" value="">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
              </div>
              <div class="card card-success">
                <div class="card-header">
                  <h3 class="card-title">Availability</h3>
                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fas fa-minus"></i></button>
                  </div>
                </div>
                <div class="card-body">
                  <div class="form-group">
                    <div class="row">
                      <div class="col-md-6">
                        <label for="inputAvailable">Unit Available</label>
                        <div class="input-group">
                          <input required type="number" name="available" class="form-control" name="" value="">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <label for="inputAvailable">Project Status</label>
                        <div class="input-group">
                          <select required id="project_status" name="project_status" class="form-control custom-select">
                            <option selected disabled>Select one</option>
                            <option value="success">Success</option>
                            <option value="recent">Recent</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-md-6">
                        <label for="is_active">Is Active</label>
                        <div class="input-group">
                          <input required type="checkbox" id="is_active" name="is_active" checked data-bootstrap-switch data-off-color="danger" data-on-color="success">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <label for="is_slideshow">Is Slideshow</label>
                        <div class="input-group">
                          <input required type="checkbox" id="is_slideshow" name="is_slideshow" checked data-bootstrap-switch data-off-color="danger" data-on-color="success">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card -->
            </div>
          </div>
          <div class="row">
            <div class="col-12">
              <a href="#" class="btn btn-secondary">Cancel</a>
              <input type="submit" value="Create new Porject" class="btn btn-success float-right">
            </div>
          </div>
        </form>
      </section>
      <br>
      <!-- /.content -->
    </div>

    @endsection

    @section('js')
    <script type="text/javascript">

    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
          $('#image_upload_preview').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
      }
    }

    $("#cover").change(function () {
      readURL(this);
    });

    function initAutocomplete() {
      var myLatlng = new google.maps.LatLng(-6.229728,106.6894284);
      var map = new google.maps.Map(document.getElementById('googleMap'), {
        center: myLatlng,
        zoom: 13,
        mapTypeId: 'roadmap'
      });

      // Create the search box and link it to the UI element.
      var input = document.getElementById('pac-input');
      var searchBox = new google.maps.places.SearchBox(input);
      map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

      // Bias the SearchBox results towards current map's viewport.
      map.addListener('bounds_changed', function() {
        searchBox.setBounds(map.getBounds());
      });

      var markers = [];
      // Listen for the event fired when the user selects a prediction and retrieve
      // more details for that place.
      searchBox.addListener('places_changed', function() {
        var places = searchBox.getPlaces();

        if (places.length == 0) {
          return;
        }

        // Clear out the old markers.
        markers.forEach(function(marker) {
          marker.setMap(null);
        });
        markers = [];

        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();
        places.forEach(function(place) {
          if (!place.geometry) {
            console.log("Returned place contains no geometry");
            return;
          }
          var icon = {
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(25, 25)
          };

          // Create a marker for each place.
          var marker = new google.maps.Marker({
            map: map,
            title: place.name,
            icon: '{{asset("/img/pin.png")}}',
            position: place.geometry.location,
            draggable: true
          });

          document.getElementById('lat').value = place.geometry.location.lat();
          document.getElementById('lng').value = place.geometry.location.lng();

          google.maps.event.addListener(marker,'dragend',function(event) {
            document.getElementById('lat').value = event.latLng.lat();
            document.getElementById('lng').value = event.latLng.lng();
          });

          if (place.geometry.viewport) {
            // Only geocodes have viewport.
            bounds.union(place.geometry.viewport);
          } else {
            bounds.extend(place.geometry.location);
          }
        });
        map.fitBounds(bounds);
      });
    }

    </script>
    <script type="text/javascript">
      $('.input-images-1').imageUploader();

      $('.uploaded-image button').hide();

      $('.uploaded-image').on('click', function(){
        $(this).find('button').trigger('click');
      });
    </script>
    <script type="text/javascript">
      $("input[data-bootstrap-switch]").each(function(){
        $(this).bootstrapSwitch('state', $(this).prop('checked'));
      });
    </script>
    @endsection
