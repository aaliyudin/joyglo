@extends('layouts.dashboard')
@section('css')
  <style media="screen">
    .image-uploader .uploaded .uploaded-image .delete-image {
      display: block !important;
    }

    #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 400px;
        top: 55px !important;
        left: 35px !important;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }
  </style>
@endsection
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Project Edit</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item active"><a href="{{route('dashboard-projects')}}">Projects</a></li>
            <li class="breadcrumb-item active">Edit</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <form class="" action="{{route('dashboard-projects-update')}}" method="post" enctype="multipart/form-data">
      @csrf
      <input type="hidden" name="id" value="{{$project->id}}">
      <div class="row">
        <div class="col-md-6">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">General</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fas fa-minus"></i></button>
                </div>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label for="name">Project Name</label>
                  <input type="text" id="name" name="name" value="{{$project->name}}" class="form-control">
                </div>
                <div id="googleMap" style="height: 400px;"></div>
                <input id="pac-input" class="controls" type="text" placeholder="Search Box">
                <input type='hidden' value="{{$project->latitude}}" name='latitude' id='lat'>
                <input type='hidden' value="{{$project->longitude}}" name='longitude' id='lng'>
                <div class="form-group">
                  <label for="address">Project Address</label>
                  <textarea id="address" name="address" class="form-control" rows="3">{{$project->address}}</textarea>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-6">
                      <label for="country">Country</label>
                      <select id="country" name="country" class="form-control custom-select">
                        <option disabled>Select one</option>
                        @foreach($country as $c)
                        <option value="{{$c->name}}" {{ $c->name == $project->country ? 'selected' : '' }}>{{$c->name}}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="col-md-6">
                      <label for="city">City</label>
                      <select id="city" name="city" class="form-control custom-select">
                        <option disabled>Select one</option>
                        @foreach($city as $c)
                        <option value="{{$c->name}}" {{ $c->name == $project->cities ? 'selected' : '' }}>{{$c->name}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="description">Project Description</label>
                  <textarea id="description" name="description" class="form-control textarea" rows="4">{{$project->description}}</textarea>
                </div>
                <div class="form-group">
                  <label for="status">Property Status</label>
                  <select id="status" name="status" class="form-control custom-select">
                    <option disabled>Select one</option>
                    <option {{ 'Sale' == $project->status ? 'selected' : '' }}>Sale</option>
                    <option {{ 'Rent' == $project->status ? 'selected' : '' }}>Rent</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="type">Property Type</label>
                  <select id="type" name="type" class="form-control custom-select">
                    <option selected disabled>Select one</option>
                    <option {{ 'House' == $project->type ? 'selected' : '' }}>House</option>
                    <option {{ 'Apartment' == $project->type ? 'selected' : '' }}>Apartment</option>
                  </select>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <div class="col-md-6">
            <div class="card card-secondary">
              <div class="card-header">
                <h3 class="card-title">Detail</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                  </div>
                </div>
                <div class="card-body">
                  <div class="form-group">
                    <label for="inputImage">Cover Image</label>
                    <input type="file" id="cover" name="cover" accept=".jpg,.jpeg,.png,.gif,.bmp,.tiff">
                    <br>
                    @if(strlen($project->cover) > 0)
                    <img id="image_upload_preview" src="{{$project->cover}}" alt="your image" height="200"/>
                    @else
                    <img id="image_upload_preview" src="{{$project->images[0]->source}}" alt="your image" height="200"/>
                    @endif
                  </div>
                  <div class="form-group">
                      <label class="active">Photos</label>
                      <div class="input-images-1" style="padding-top: .5rem;"></div>
                      <?php $i = 0 ?>
                      @foreach($project->images as $image)
                      <input type="hidden" class="input_image" name="input_image_{{$i}}" data-id="{{$image->id}}" value="{{$image->source}}">
                      <?php $i++ ?>
                      @endforeach
                  </div>
                  <div class="form-group">
                    <label for="inputStatus">Property Price</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Rp</span>
                      </div>
                      <input type="text" name="price" value="{{$project->price}}" class="form-control">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-md-6">
                        <label for="inputBedroom">Bedroom(s)</label>
                        <input type="text" name="bedrooms" class="form-control" name="" value="{{$project->bedrooms}}">
                      </div>
                      <div class="col-md-6">
                        <label for="inputBathroom">Bathroom(s)</label>
                        <input type="text" name="bathrooms" class="form-control" name="" value="{{$project->bathrooms}}">
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-md-6">
                        <label for="inputLand">Land Area</label>
                        <div class="input-group">
                          <input type="number" name="area" class="form-control" name="" value="{{$project->area}}">
                          <div class="input-group-append">
                            <span class="input-group-text">m<sup>2</sup></span>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <label for="inputLand">Building Area</label>
                        <div class="input-group">
                          <input type="number" name="building" class="form-control" name="" value="{{$project->building}}">
                          <div class="input-group-append">
                            <span class="input-group-text">m<sup>2</sup></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-md-6">
                        <label for="garages">Garages / Carpot</label>
                        <div class="input-group">
                          <input type="number" name="garages" class="form-control" name="" value="{{$project->garages}}">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-md-6">
                        <label for="inputYear">Year Built</label>
                        <div class="input-group">
                          <input type="number" name="year" class="form-control" name="" value="{{$project->year}}">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
              </div>
              <div class="card card-success">
                <div class="card-header">
                  <h3 class="card-title">Availability</h3>
                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fas fa-minus"></i></button>
                  </div>
                </div>
                <div class="card-body">
                  <div class="form-group">
                    <div class="row">
                      <div class="col-md-6">
                        <label for="inputAvailable">Unit Available</label>
                        <div class="input-group">
                          <input type="number" name="available" class="form-control" name="" value="{{$project->available}}">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <label for="inputAvailable">Project Status</label>
                        <div class="input-group">
                          <select required id="project_status" name="project_status" class="form-control custom-select">
                            <option selected disabled>Select one</option>
                            <option {{ 'success' == $project->project_status ? 'selected' : '' }} value="success">Success</option>
                            <option {{ 'recent' == $project->project_status ? 'selected' : '' }} value="recent">Recent</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-md-6">
                        <label for="is_active">Is Active</label>
                        <div class="input-group">
                          <input type="checkbox" id="is_active" name="is_active" checked data-bootstrap-switch data-off-color="danger" data-on-color="success">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <label for="is_slideshow">Is Slideshow</label>
                        <div class="input-group">
                          <input type="checkbox" id="is_slideshow" name="is_slideshow" checked data-bootstrap-switch data-off-color="danger" data-on-color="success">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card -->
            </div>
          </div>
          <div class="row">
            <div class="col-12">
              <a href="{{route('dashboard-projects')}}" class="btn btn-secondary">Cancel</a>
              <input type="submit" value="Edit Porject" class="btn btn-success float-right">
            </div>
          </div>
        </form>
      </section>
      <br>
      <!-- /.content -->
    </div>

    <div class="modal fade" id="modal-default">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Delete image</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p>Are you sure to delete this image?</p>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-danger" id="delete-image" data-id="0">Delete</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>

    @endsection

    @section('js')
    <script type="text/javascript">

    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
          $('#image_upload_preview').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
      }
    }

    $("#cover").change(function () {
      readURL(this);
    });

    function initAutocomplete() {
      var latitude = document.getElementById('lat').value;
      var longitude = document.getElementById('lng').value;
      var myLatlng = new google.maps.LatLng(latitude,longitude);
      var map = new google.maps.Map(document.getElementById('googleMap'), {
        center: myLatlng,
        zoom: 18,
        mapTypeId: 'roadmap'
      });

      var markerizo = new google.maps.Marker({
        position: myLatlng,
        map: map,
        icon: '{{asset("/img/pin.png")}}',
        draggable:true
      });

      // Create the search box and link it to the UI element.
      var input = document.getElementById('pac-input');
      var searchBox = new google.maps.places.SearchBox(input);
      map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

      // Bias the SearchBox results towards current map's viewport.
      map.addListener('bounds_changed', function() {
        searchBox.setBounds(map.getBounds());
      });

      var markers = [];
      // Listen for the event fired when the user selects a prediction and retrieve
      // more details for that place.
      searchBox.addListener('places_changed', function() {
        var places = searchBox.getPlaces();

        if (places.length == 0) {
          return;
        }

        // Clear out the old markers.
        markers.forEach(function(marker) {
          marker.setMap(null);
        });
        markers = [];

        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();
        places.forEach(function(place) {
          if (!place.geometry) {
            console.log("Returned place contains no geometry");
            return;
          }
          var icon = {
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(25, 25)
          };

          // Create a marker for each place.
          var marker = new google.maps.Marker({
            map: map,
            title: place.name,
            icon: '{{asset("/img/pin.png")}}',
            position: place.geometry.location,
            draggable: true
          });

          document.getElementById('lat').value = place.geometry.location.lat();
          document.getElementById('lng').value = place.geometry.location.lng();

          google.maps.event.addListener(marker,'dragend',function(event) {
            document.getElementById('lat').value = event.latLng.lat();
            document.getElementById('lng').value = event.latLng.lng();
          });

          if (place.geometry.viewport) {
            // Only geocodes have viewport.
            bounds.union(place.geometry.viewport);
          } else {
            bounds.extend(place.geometry.location);
          }
        });
        map.fitBounds(bounds);
      });
    }

    </script>
    <script type="text/javascript">
    var c_input_img = $('.input_image').length;
    var imgs = new Array();
    for (var i = 0; i < c_input_img; i++) {
      imgs[i]= new Array();
      imgs[i]['src'] = $("input[name=input_image_"+i+"]").val();
      imgs[i]['id'] = $("input[name=input_image_"+i+"]").attr('data-id');
    }
    let preloaded = imgs;

    $('.input-images-1').imageUploader({
      preloaded: preloaded
    });

    $('.delete-image').on('click', function(){
      var id = $(this).nextAll("input").val();
      var data = {
        '_token': '{{ csrf_token() }}',
        'id'    : id
      };
      var url = '{{route('dashboard-projects-image-erase')}}';
      $.ajax(
        {
          type    : 'POST',
          url     : url,
          data    : data,
          success : function(response)
          {
            $('#modal-default').modal('hide');
          }
        });
    })

    // $('.uploaded-image button').hide();

    // $('.uploaded-image').on('click', function(){
    //   var inpv = $(this).find('input').val();
    //   $('#delete-image').prop('data-id', inpv);
    //   console.log(inpv);
    //   $('#modal-default').modal();
    // });
    //
    // $('#delete-image').on('click', function(){
    //   var data = {
    //     '_token': '{{ csrf_token() }}',
    //     'id'    : $(this).prop('data-id')
    //   };
    //   var url = '{{route('dashboard-projects-image-erase')}}';
    //   $.ajax(
    //     {
    //       type    : 'POST',
    //       url     : url,
    //       data    : data,
    //       success : function(response)
    //       {
    //         $('#modal-default').modal('hide');
    //       }
    //     });
    // })
    </script>
    <script type="text/javascript">
      $("input[data-bootstrap-switch]").each(function(){
        $(this).bootstrapSwitch('state', $(this).prop('checked'));
      });
    </script>
    @endsection
