@extends('layouts.dashboard')

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Projects</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Projects</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <section class="content">

    <!-- Default box -->
    <div class="card">
      <div class="card-header">
        <div class="row">
          <div class="col-md-6">
            <h3 class="card-title">Projects List</h3>
          </div>
          <div class="col-md-6">
            <a class="btn btn-success btn-sm float-right" href="{{route('dashboard-projects-add')}}">
              <i class="fas fa-plus">
              </i>
              Add
            </a>
          </div>
        </div>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <table id="projects-table" class="table table-bordered table-striped">
          <thead>
          <tr>
            <th>Project Name</th>
            <th>Location</th>
            <th>Available</th>
            <th>Price</th>
            <th>Action</th>
          </tr>
          </thead>
          <tbody>
          <tr>
            <td>Joyglo Tebet</td>
            <td>Tebet</td>
            <td>20</td>
            <td>Rp. 1.200.000.000</td>
            <td class="project-actions text-right">
              <a class="btn btn-primary btn-sm" href="#">
                <i class="fas fa-eye">
                </i>
              </a>
              <a class="btn btn-info btn-sm" href="#">
                <i class="fas fa-pencil-alt">
                </i>
              </a>
              <a class="btn btn-danger btn-sm" href="#">
                <i class="fas fa-trash">
                </i>
              </a>
            </td>
          </tr>
          </tbody>
          <tfoot>
          <tr>
            <th>Project Name</th>
            <th>Location</th>
            <th>Available</th>
            <th>Price</th>
            <th>Action</th>
          </tr>
          </tfoot>
        </table>
      </div>
      <!-- /.card-body -->
    </div>

  </section>
</div>

<div class="modal fade" id="modal-sm">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Delete Project</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Are you sure to delete this?</p>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="delete-project" data-id="0">Delete</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

@endsection

@section('js')
<script>
$(function() {
    $('#projects-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{route('dashboard-projects-list')}}',
        columns: [
            { data: 'name', name: 'name' },
            { data: 'address', name: 'address' },
            { data: 'available', name: 'available' },
            { data: 'price', name: 'price' },
            {
              "data": null,
              "sortable": false,
              "render": function ( data, type, full, meta ) {
                    var buttonEdit = "{{route('dashboard-projects-edit')}}?id="+full.id;
                     return "<a class='btn btn-info btn-sm' href='"+buttonEdit+"'>"+
                       "<i class='fas fa-pencil-alt'>"+
                       "</i>"+
                     "</a>"+
                     "<button class='btn btn-danger btn-sm delete-id' data-id='"+full.id+"' href='#'>"+
                       "<i class='fas fa-trash'>"+
                       "</i>"+
                     "</button>"
                 }
            }
        ],
        initComplete : function( settings, json ) {
          $('.delete-id').on('click', function(){
            var id = $(this).attr('data-id');
            $('#delete-project').attr('data-id', id);
            $('#modal-sm').modal();
          });

          $('#delete-project').on('click', function(){
            var id = $(this).attr('data-id');
            var data = {
              '_token': '{{ csrf_token() }}',
              'id'    : id
            };
            var url = '{{route('dashboard-projects-delete')}}';
            $.ajax(
              {
                type    : 'POST',
                url     : url,
                data    : data,
                success : function(response)
                {
                  $('#modal-sm').modal('hide');
                  var table = $('#projects-table').DataTable();
                  table.ajax.reload();
                }
              });
          });
        }
    });


});
</script>
@endsection
