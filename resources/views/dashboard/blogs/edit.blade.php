@extends('layouts.dashboard')

@section('css')
<link rel="stylesheet" href="{{asset('dashboard/dist/css/croppie.css')}}" crossorigin="anonymous">
@endsection

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Blog Edit</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item active"><a href="{{route('dashboard-projects')}}">Blogs</a></li>
            <li class="breadcrumb-item active">Edit</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <form class="" action="{{route('dashboard-blogs-input')}}" method="post" enctype="multipart/form-data">
      @csrf
      <div class="row">
        <div class="col-md-12">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">General</h3>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fas fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="card-body">
              <div class="form-group">
                <label for="name">Blog Title</label>
                <input type="text" id="title" name="title" value="{{$blog->title}}" class="form-control">
              </div>
              <div class="form-group">
                <label for="images">Image</label>
                <input type="file" id="images" name="images" accept=".jpg,.jpeg,.png,.gif,.bmp,.tiff">
                <br>
                <img id="image_upload_preview" src="{{$blog->images}}" alt="your image" height="200"/>
              </div>
              <div class="form-group">
                <div class="mb-3">
                  <textarea name="content" class="textarea" placeholder="Place some text here">{{$blog->content}}</textarea>
                </div>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>

      </div>
      <div class="row">
        <div class="col-12">
          <a href="#" class="btn btn-secondary">Cancel</a>
          <input type="submit" value="Edit Blog" class="btn btn-success float-right">
        </div>
      </div>
    </form>
  </section>
  <br>
  <!-- /.content -->
</div>

@endsection

@section('js')
<script type="text/javascript">
  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#image_upload_preview').attr('src', e.target.result);
      }

      reader.readAsDataURL(input.files[0]);
    }
  }

  $("#images").change(function () {
    readURL(this);
  });
</script>
@endsection
